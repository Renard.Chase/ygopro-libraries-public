-- Vertex's Scripting Library
dofile "ext/extensions.lua"
if not Extensions then
  Debug.Message("init.lua requires Extension \"Extensions\" of at least version \"1.0\" to load Extensions.")
  Extensions.Loaded=false
  return
end

function ext(file)
  if not Extensions.Loaded then return false end
  local err,msg=pcall(dofile,file)
  if not err then
    Debug.Message(msg)
    Extensions.Loaded=false
  end
  return err
end

ext("ext/type.lua")
ext("ext/maybe.lua")
ext("ext/io.lua")
ext("ext/string.lua")
ext("ext/function.lua")
ext("ext/short-function.lua")
ext("ext/math.lua")
ext("ext/config.lua")
ext("ext/constant.lua")
ext("ext/logical.lua")
ext("ext/id.lua")
ext("ext/short-id.lua")
ext("ext/effect.lua")
ext("ext/auto-effects.lua")
ext("ext/data-effects.lua")
ext("ext/ignore.lua")
ext("ext/group.lua")
ext("ext/card.lua")
ext("ext/debug.lua")
ext("ext/short-debug.lua")
ext("ext/table.lua")
ext("ext/duel.lua")
ext("ext/short-duel.lua")
ext("ext/chain-data.lua")
ext("ext/ritual.lua")
ext("ext/set.lua")
ext("ext/duel-mode.lua")
ext("ext/puzzle.lua")
ext("ext/info.lua")
ext("ext/pen-fix.lua")
ext("ext/card-data.lua")
ext("ext/builder-effect.lua")
ext("ext/errata.lua")
ext("ext/rp-chars.lua")
ext("ext/effect-maybe.lua")
ext("ext/drive-point.lua")
Extensions.Check()
