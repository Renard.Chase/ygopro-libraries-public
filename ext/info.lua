-- Vertex's Scripting Library
local x="info"
Extensions.Register(x,1,0,"In Duel Information Notes")
Extensions.Require(x,"config",1,0)
Extensions.Require(x,"effect",1,3)
Extensions.Require(x,"duel",1,3)
Extensions.Require(x,"group",1,3)
Extensions.Require(x,"function",1,5)


if not Config.HasValue("info-card-id") then return end
local ready=true
local id =Config.GetIntegerOrDefault("info-card-id","-1")
local ns =Config.GetIntegerOrDefault("info-normal-summon-str",0)
local fs =Config.GetIntegerOrDefault("info-flip-summon-str",0)
local ss =Config.GetIntegerOrDefault("info-special-summon-str",0)
local set=Config.GetIntegerOrDefault("info-set-str",0)
local atk=Config.GetIntegerOrDefault("info-attacker-str",0)
local atg=Config.GetIntegerOrDefault("info-attack-target-str",0)
local ad =Config.GetIntegerOrDefault("info-attack-direct-str",0)
local ac =Config.GetIntegerOrDefault("info-activate-str",0)
local atd=Config.GetIntegerOrDefault("info-attacked-str",0)

local flags=NO_COPY_NO_DISABLE+EFFECT_FLAG_IMMEDIATELY_APPLY+EFFECT_FLAG_IGNORE_IMMUNE
local ce=Effect.CreateEffect

local function f(n)
  return function(e,tp,eg,ep,ev,re,r,rp)
    if not eg or eg:IsEmpty() then return end
    local tc=eg:GetFirst()
    while tc do
      local e1=ce(tc)
      e1:SetDescription(aux.Stringid(id,n))
      e1:SetType(EFFECT_TYPE_SINGLE)
      e1:SetProperty(flags+EFFECT_FLAG_CLIENT_HINT)
      e1:ResetEndOrLeave()
      tc:RegisterEffect(e1)
      tc=eg:GetNext()
    end
  end
end

function Effect.CreateEffect(c,...)
  if ready and id>0 then
    local e1=Effect.GlobalEffect()
    e1:SetActivation(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS,EVENT_SUMMON_SUCCESS)
    e1:SetProperty(flags)
    e1:SetOperation(f(ns))
    Duel.RegisterEffect(e1,0)
    local e2=e1:Clone(EVENT_FLIP_SUMMON_SUCCESS)
    e2:SetOperation(f(fs))
    Duel.RegisterEffect(e2,0)
    local e3=e1:Clone(EVENT_SPSUMMON_SUCCESS)
    e3:SetOperation(f(ss))
    Duel.RegisterEffect(e3,0)
    local e4=e1:Clone(EVENT_MSET)
    e4:SetOperation(f(set))
    Duel.RegisterEffect(e4,0)
    Duel.RegisterClone(e4,0,EVENT_SSET)
    local e5=e1:Clone(EVENT_ATTACK_ANNOUNCE)
    e5:SetOperation(function (e,tp,eg,ep,ev,re,r,rp)
      local a,d=Duel.GetAttackCards()
      if d then
        local x1=ce(a)
        x1:SetDescription(aux.Stringid(id,atk))
        x1:SetType(EFFECT_TYPE_SINGLE)
        x1:SetProperty(flags+EFFECT_FLAG_CLIENT_HINT)
        x1:SetReset(RESET_PHASE+PHASE_DAMAGE_CAL+RESET_EVENT+RESET_STANDARD)
        a:RegisterEffect(x1)
        x1:Clone()
          :SetDescription(aux.Stringid(id,atd))
          :SetReset(RESET_END_STANDARD)
          :Register(a)
        local x2=ce(d)
        x2:SetDescription(aux.Stringid(id,atg))
        x2:SetType(EFFECT_TYPE_SINGLE)
        x2:SetProperty(flags+EFFECT_FLAG_CLIENT_HINT)
        x2:SetReset(RESET_PHASE+PHASE_DAMAGE_CAL+RESET_EVENT+RESET_STANDARD)
        d:RegisterEffect(x2)
      else
        local x1=ce(a)
        x1:SetDescription(aux.Stringid(id,ad))
        x1:SetType(EFFECT_TYPE_SINGLE)
        x1:SetProperty(flags+EFFECT_FLAG_CLIENT_HINT)
        x1:SetReset(RESET_PHASE+PHASE_DAMAGE_CAL+RESET_EVENT+RESET_STANDARD)
        a:RegisterEffect(x1)
      end
    end)
    Duel.RegisterEffect(e5,0)
    local e6=e1:Clone(EVENT_CHAIN_ACTIVATING)
    -- e6:SetCondition(function (e,tp,eg,ep,ev,re,r,rp) return re and re:GetHandler() and not re:GetHandler():IsMonster() end)
    e6:SetOperation(function (e,tp,eg,ep,ev,re,r,rp)
      if not re or not re:GetHandler() or re:GetHandler():IsOffField() then return end
      local rc=re:GetHandler()
      local x1=ce(rc)
      x1:SetDescription(aux.Stringid(id,ac))
      x1:SetType(EFFECT_TYPE_SINGLE)
      x1:SetProperty(flags+EFFECT_FLAG_CLIENT_HINT)
      x1:ResetEndOrLeave()
      rc:RegisterEffect(x1)
    end)
    Duel.RegisterEffect(e6,0)
    ready=false
  end
  return ce(c,...)
end
