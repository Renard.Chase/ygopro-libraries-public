-- Vertex's Scripting Library
Extensions.Register("logical",1,0,"Logical")

Logical={}
function Logical.And(...)
  local args=table.pack(...)
  for n=0,args.n do
    if not args[n] then return false end
  end
  return true
end

function Logical.Or(...)
  local args=table.pack(...)
  for n=0,args.n do
    if args[n] then return true end
  end
  return false
end
