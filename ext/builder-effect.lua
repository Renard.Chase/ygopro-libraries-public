-- Vertex's Scripting Library
Extensions.Register("effect-builder",1,0,"Effect Builder Pattern")

local makeBuilder=function (f)
  local func=f
  return function (e,...)
    local chk,msg=pcall(func,e,...)
    if not chk then error(msg,2) end
    return e
  end
end
local mb=makeBuilder

Effect.SetDescription=mb(Effect.SetDescription)
Effect.SetCategory=mb(Effect.SetCategory)
Effect.SetType=mb(Effect.SetType)
Effect.SetCode=mb(Effect.SetCode)
Effect.SetProperty=mb(Effect.SetProperty)
Effect.SetRange=mb(Effect.SetRange)
Effect.SetAbsoluteRange=mb(Effect.SetAbsoluteRange)
Effect.SetCountLimit=mb(Effect.SetCountLimit)
Effect.SetReset=mb(Effect.SetReset)
Effect.SetLabel=mb(Effect.SetLabel)
Effect.SetLabelObject=mb(Effect.SetLabelObject)
Effect.SetHintTiming=mb(Effect.SetHintTiming)
Effect.SetCondition=mb(Effect.SetCondition)
Effect.SetCost=mb(Effect.SetCost)
Effect.SetTarget=mb(Effect.SetTarget)
Effect.SetTargetRange=mb(Effect.SetTargetRange)
Effect.SetValue=mb(Effect.SetValue)
Effect.SetOperation=mb(Effect.SetOperation)
Effect.SetOwnerPlayer=mb(Effect.SetOwnerPlayer)
