-- Vertex's Scripting Library
Extensions.Register("chain-data",1,1,"Chain Data")
Extensions.Require("chain-data","maybe",1,0)
local data={}
local turn=-1
local function init_data()
  --local t=Duel.GetTurnID()
  local c=Duel.GetChainInfo(0,CHAININFO_CHAIN_ID)
  --if t~=turn then data={} turn=t end
  if not data[c] then data[c]={} end
  return c
end

function Duel.SetChainData(key,value)
  local c=init_data() 
  data[c][key]=value
end

function Duel.GetChainData(key)
  local c=init_data()
  return data[c][key]
end

function Duel.PackChainData(key,...)
  local c=init_data()
  data[c][key]=table.pack(...)
end

function Duel.UnpackChainData(key)
  local c=init_data()
  return table.unpack(data[c][key])
end

function Duel.HasChainData(key)
  local c=init_data()
  return data[c][key]~=nil
end

function Duel.MaybeGetChainData(key)
  if Duel.HasChainData(key) then
    return Maybe.Just(Duel.GetChainData(key))
  else
    return Maybe.Nothing()
  end
end

function Duel.GetChainDataOrDefault(key,def)
  return Duel.HasChainData(key) and Duel.GetChainData(key) or def
end

function Duel.GetChainDataInfo()
  if not IsDebugMode() then error("Chain Data Error : Duel.GetChainDataInfo, debug flag must be enabled.",2) end
  return data
end
