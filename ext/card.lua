local src="card"
Extensions.Register(src,1,7,"card")
Extensions.Require(src,"type",1,0)
Extensions.Require(src,"function",1,0)
Extensions.Require(src,"math",1,0)
Extensions.Require(src,"maybe",1,0)
Extensions.Require(src,"constant",1,1)
function Card.Destroy(g,r,d)
  r=r or REASON_EFFECT
  return Duel.Destroy(g,r,d)
end

function Card.Banish(g,p,r)
  r=r or REASON_EFFECT
  p=p or POS_FACEUP
  return Duel.Remove(g,p,r)
end

function Card.BanishAsCost(g,p,r)
  r=(r or 0)+REASON_COST
  return g:Banish(p,r)
end

function Card.IsNotImmuneToEffect(c,e)
  return not c:IsImmuneToEffect(e)
end

function Card.SendtoGrave(target,reason)
  reason=reason or REASON_EFFECT
  return Duel.SendtoGrave(target,reason)
end

function Card.SendtoGraveAsCost(target,reason)
  reason=(reason or 0)+REASON_COST
  target:SendtoGrave(reason)
end

function Card.Discard(target,reason)
  reason=reason or REASON_EFFECT
  return target:SendtoGrave(reason+REASON_DISCARD)
end

function Card.DiscardAsCost(target,reason)
  reason=(reason or 0)+REASON_DISCARD
  return target:SendtoGraveAsCost(reason)
end

function Card.SendtoHand(target,player,reason)
  reason=reason or REASON_EFFECT
  return Duel.SendtoHand(target,player,reason)
end

function Card.ConfirmtoHand(target,player,reason)
  if type(target)=="number" then return CC(Card.ConfirmtoHand,target,player) end
  local n=target:SendtoHand(player,reason)
  target=Duel.GetOperatedGroup()
  if player~=nil then
    target:ConfirmCards(1-player)
  else
    target:ConfirmCards()
  end
  return n
end

function Card.SendtoHandAsCost(target,player,reason)
  reason=(reason or 0)+REASON_COST
  target:SendtoHand(player,reason)
end

function Card.SendtoDeck(target,player,seq,reason)
  seq=seq or 0
  reason=reason or REASON_EFFECT
  return Duel.SendtoDeck(target,player,seq,reason)
end

function Card.SendtoDeckAsCost(target,player,seq,reason)
  reason=(reason or 0)+REASON_COST
  return target:SendtoDeck(player,seq,reason)
end

function Card.ShuffletoDeck(target,player,reason)
  reason=reason or REASON_EFFECT
  return target:SendtoDeck(player,DECK_ORDER_SHUFFLE,reason)
end

function Card.ShuffletoDeckAsCost(target,player,reason)
  reason=(reason or 0)+REASON_COST
  return target:ShuffletoDeck(player,reason)
end

function Card.Release(c,r)
  r=r or REASON_COST
  return Duel.Release(c,r)
end

function Card.SpecialSummon(target,sumtype,sumplayer,target_player,nocheck,nolimit,pos)
  if type(target)=="number" then return function (tc) tc:SpecialSummon(target,sumtype,sumplayer,target_player,nocheck,nolimit) end end
  target_player=target_player or sumplayer
  nocheck=nocheck or false
  nolimit=nolimit or false
  pos=pos or POS_FACEUP
  return Duel.SpecialSummon(target,sumtype,sumplayer,target_player,nocheck,nolimit,pos)
end

function Card.SpecialSummonStep(target,sumtype,sumplayer,target_player,nocheck,nolimit,pos)
  if type(target)=="number" then return function (tc) tc:SpecialSummonStep(target,sumtype,sumplayer,target_player,nocheck,nolimit) end end
  target_player=target_player or sumplayer
  nocheck=nocheck or false
  nolimit=nolimit or false
  pos=pos or POS_FACEUP
  return Duel.SpecialSummonStep(target,sumtype,sumplayer,target_player,nocheck,nolimit,pos)
end

function Card.Overlay(c,tg)
  if type(tg)=="card" then
    tg=Group.FromCards(tg)
  end
  Duel.Overlay(c,tg)
end
Card.Attach=Card.Overlay

local roc=Card.RemoveOverlayCard
function Card.RemoveOverlayCard(c,p,min,max,r)
  p=p or c:GetControler()
  min=min or 1
  max=max or min
  r=r or REASON_COST
  return roc(c,p,min,max,r)
end
Card.Detach=Card.RemoveOverlayCard

local croc=Card.CheckRemoveOverlayCard
function Card.CheckRemoveOverlayCard(c,tp,n,r)
  tp=tp or c:GetControler()
  n=n or 1
  r=r or REASON_COST
  return croc(c,tp,n,r)
end

function Card.RemoveOverlayCost(mn,mx,r,o)
  mn=mn or 1
  mx=mx or mn
  r=r or REASON_COST
  return function(e,tp,eg,ep,ev,re,r,rp,chk)
    local c=e:GetHandler()
    local p=o and 1-tp or tp
    if chk==0 then
      return c:CheckRemoveOverlayCard(p,mn,r)
    end
    Duel.HintSM(p,HINTMSG_REMOVEXYZ)
    c:RemoveOverlayCard(p,mn,mx,r)
  end
end

function Card.HasSummonType(c,...)
  local t=0
  for _,v in pairs(...) do t=t+v end
  return bit.band(c:GetSummonType(),v)>0
end

function Card.IsAttack(c,atk)
  return c:GetAttack()==atk
end

function Card.IsDefence(c,def)
  return c:GetDefence()==def
end

Card.IsDefenceAbove=Card.IsDefenseAbove
Card.IsDefenceBelow=Card.IsDefenseBelow
Card.IsDefense=Card.IsDefence
Card.GetDefence=Card.GetDefense
Card.GetBaseDefence=Card.GetBaseDefense
Card.GetTextDefence=Card.GetTextDefense
Card.GetPreviousDefenceOnField=Card.GetPreviousDefenseOnField
Card.IsDefencePos=Card.IsDefensePos

function Card.IsLevel(c,level)
  local hasLevel=c:HasLevel()
  local isLevel=false
  if hasLevel then
    isLevel=c:GetLevel()==level
  end
  return isLevel,hasLevel
end

function Card.IsRank(c,rank)
  local hasRank=not c:HasLevel()
  local isRank=false
  if hasRank then
    isRank=c:GetRank()==rank
  end
  return isRank,hasRank
end

function Card.HasLevel(c)
  return c:IsLevelAbove(1) and c:IsNotStatus(STATUS_NO_LEVEL)
end

function Card.GetLevelOrRank(c,use_rank)
  local lv=c:GetLevel()
  local rk=c:GetRank()
  if lv>0 and rk>0 and use_rank then lv=rk end
  return lv>0 and lv or rk
end

function Card.IsEqual(c1,c2)
  return c1==c2
end

function Card.ConfirmCards(c,tp)
  if type(c)=="number" then return CC(Card.ConfirmCards,c) end
  local tp=tp or 1-c:GetControler()
  return Duel.ConfirmCards(tp,Group.FromCards(c))
end

local uof=Card.SetUniqueOnField
function Card.SetUniqueOnField(c,s,o,id)
  s=s or 1
  o=o or 0
  id=id or c:GetOriginalCode()
  return uof(c,s,o,id)
end

function Card.RegisterClone(c,e,...)
  local cl=e:Clone(...)
  c:RegisterEffect(cl)
  return cl
end

function Card.RegisterClones(c,...)
  if type(c)=="effect" then return CC(Card.RegisterClones,c,...) end
  local args=table.pack(...)
  for n=1,args.n do
    c:RegisterClone(args[n])
  end
end

function Card.MoveToField(c,mp,tp,dest,pos,enabled)
  tp=tp or mp
  local t=c:IsType(TYPE_MONSTER)
  dest=dest or (t and LOCATION_MZONE or LOCATION_SZONE)
  pos=pos or (dest==LOCATION_MZONE and POS_FACEUP_ATTACK or POS_FACEUP)
  enabled=enabled==nil or enabled
  Duel.MoveToField(c,mp,tp,dest,pos,enabled)
end

function Card.IsMonster(c)
  return c:IsType(TYPE_MONSTER)
end

local ire=Card.IsRelateToEffect
function Card.IsRelateToEffect(c,...)
  local args=table.pack(...)
  for n=1,args.n do
    if not ire(c,args[n]) then return false end
  end
  return true
end

function Card.IsNotRelateToEffect(c,...)
  return not c:IsRelateToEffect(...)
end

local ic=Card.IsCode
function Card.IsCode(c,t,...)
  if type(t)=="table" then return c:IsCode(table.unpack(t)) end
  local args=table.pack(t,...)
  for n=1,args.n do
    if ic(c,args[n]) then return true end
  end
  return false
end

function Card.TargetFilterCheck(c,f,p,z,...)
  local hasFunction=(not f) or f(c,...)
  local hasControl=(not p) or c:IsControler(p)
  local isLocation=(not z) or c:IsLocation(z)
  
  return hasFunction and isLocation and hasControl
end

function Card.RegisterEffects(c,...)
  local args={...}
  for _,v in pairs(args) do
    if v then
      c:RegisterEffect(v)
    end
  end
end

function Card.IsSequence(c,seq)
  return c:GetSequence()==seq
end

function Card.ValueFunction(vf,ad,...)
  local args={...}
  return function(c)
    return ad(vf(c),table.unpack(args))
  end
end

function Card.AsGroup(c)
  return Group.FromCards(c)
end

local sm=Card.SetMaterial
function Card.SetMaterial(c,g,...)
  if type(g)=="card" then
    g=g:AsGroup()
  end
  return sm(c,g,...)
end

function Card.AddLibProc(c,lib,n,...)
  local f=n and lib.proc[n] or lib.proc
  return f(c,...)
end

local function CreateStatFunction(get,cmp)
  return function(c,val)
    return cmp(get(c),val)
  end
end

local AtkFunc=Curry(CreateStatFunction,Card.GetBaseAttack)
Card.IsBaseAttack=AtkFunc(math.equal)
Card.IsBaseAttackAbove=AtkFunc(math.greater_eq)
Card.IsBaseAttackBelow=AtkFunc(math.less_eq)

local DefFunc=Curry(CreateStatFunction,Card.GetBaseDefence)
Card.IsBaseDefence=DefFunc(math.equal)
Card.IsBaseDefenceAbove=DefFunc(math.greater_eq)
Card.IsBaseDefenceBelow=DefFunc(math.less_eq)

function Card.MaybeGetReasonCard(c)
  local M=require "lib.maybe"
  local rc=c:GetReasonCard()
  return rc and M.Just(rc) or M.Nothing()
end

function Card.IsNotStatus(...)
  return not Card.IsStatus(...)
end

function Card.IsOverlayCount(c,n)
  return c:GetOverlayCount()==n
end

function Card.HasOverlayCount(c,n)
  return c:GetOverlayCount()>=n
end

function Card.HasCounter(c,t,n)
  n=n or 1
  return c:GetCounter(t)>=n
end

function Card.SpecialSummonFilter(c,e,tp,ty,ch,ck,pos)
  ty=ty or 0
  if ch==nil then ch=false end
  if ck==nil then ck=false end
  pos=pos or POS_FACEUP
  return c:IsCanBeSpecialSummoned(e,ty,tp,ch,ck,pos)
end

function Card.IsFieldID(c,...)
  local args=table.pack(...)
  for n=1,args.n do
    if c:GetFieldID()==args[n] then return true end
  end
  return false
end

function Card.IsRealFieldID(c,...)
  local args=table.pack(...)
  for n=1,args.n do
    if c:GetRealFieldID()==args[n] then return true end
  end
  return false
end

local icbss=Card.IsCanBeSpecialSummoned
function Card.IsCanBeSpecialSummoned(c,e,sumtype,sumplayer,nocheck,nolimit,sumpos,targetplayer)
  if nocheck==nil then nocheck=false end
  if nolimit==nil then nolimit=false end

  if type(c)~="card" then error("IsCanBeSpecialSummoned : c is not a card.",3) end
  return icbss(c,e,sumtype,sumplayer,nocheck,nolimit,sumpos,targetplayer)
end

function Card.HintSelection(c)
  Duel.HintSelection(c:AsGroup())
end

function Card.DisableMaterial(c,m,r,re)
  if c==nil then
    return function (a)
      a:DisableMaterial(m,r,re)
    end
  end
  if r==nil then r=LOCATION_MZONE end
  local f=function (t) return m==true or bit.band(m,t)==t end
  local e=Effect.CreateEffect(c)
  e:SetType(EFFECT_TYPE_SINGLE)
  e:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE)
  e:SetValue(aux.TRUE)
  if r~=false then e:SetRange(LOCATION_MZONE) end
  if re then e:SetReset(re) end
  local g=function (d) local n=e:Clone() n:SetCode(d) c:RegisterEffect(n) end
  local x=function (t,d) if f(t) then g(d) end end
  x(SUMMON_TYPE_FUSION,EFFECT_CANNOT_BE_FUSION_MATERIAL)
  x(SUMMON_TYPE_SYNCHRO,EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
  x(SUMMON_TYPE_XYZ,EFFECT_CANNOT_BE_XYZ_MATERIAL)
end

function Card.GetMatchingPendulum(c,tp)
  tp=tp or c:GetControler()
  local tc=Duel.GetFieldCard(tp,LOCATION_SZONE,13-c:GetSequence())
  local hasCard=tc~=nil
  return tc,hasCard
end

function Card.MaybeGetMatchingPendulum(c,tp)
  local tc,hasCard=c:GetMatchingPendulum()
  return hasCard and Maybe.Just(tc) or Maybe.Nothing()
end

function Card.GetSummonFilter(ty,ch,lm,pos,stp)
  ty=ty or 0
  if ch==nil then ch=false end
  if lm==nil then lm=false end
  return function (c,e,tp)
    local flg,result=pcall(Card.IsCanBeSpecialSummoned,c,e,ty,tp,ch,lm,pos,stp and 1-tp or tp)
    if not flg then error(result,2) end
    return result
  end
end

--[[
  c = Card
  v = value
  p = Reset Phase (true for standard reset with no phase)
  r = origin card
  s = boolean, make uncopyable and undisableable
  cr = custom reset event
  rng = range
]]
local function makeAdjustFunction(code)
  return function(c,v,p,r,s,cr,rng)
    r=r or c
    if p==nil then p=true end
    if s==nil then s=c~=r end
    local e1=Effect.CreateEffect(r)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetCode(code)
    if --[[not]] s then e1:SetProperty(NO_COPY_NO_DISABLE) elseif s==nil then e1:SetUncopyable() end
    if cr then
      e1:SetReset(cr+(p~=nil and RESET_PHASE+p or 0))
    elseif p then
      if type(p)=="number" then
        e1:SetReset(RESET_EVENT+RESET_STANDARD+RESET_PHASE+p)
      elseif type(p)=="boolean" then
        if p then e1:SetReset(RESET_EVENT+RESET_STANDARD) end
      end
    end
    if rng then e1:SetRange(rng) end
    e1:SetValue(v)
    c:RegisterEffect(e1)
    return e1
  end
end

Card.AdjustAttack=makeAdjustFunction(EFFECT_UPDATE_ATTACK)
Card.AdjustDefense=makeAdjustFunction(EFFECT_UPDATE_DEFENSE)
Card.AdjustLevel=makeAdjustFunction(EFFECT_UPDATE_LEVEL)
Card.AdjustRank=makeAdjustFunction(EFFECT_UPDATE_RANK)
Card.SetAttack=makeAdjustFunction(EFFECT_SET_ATTACK)
Card.SetDefense=makeAdjustFunction(EFFECT_SET_DEFENSE)
Card.SetLevel=makeAdjustFunction(EFFECT_CHANGE_LEVEL)
Card.SetRank=makeAdjustFunction(EFFECT_CHANGE_RANK)
Card.LockAttack=makeAdjustFunction(EFFECT_SET_ATTACK_FINAL)
Card.LockDefense=makeAdjustFunction(EFFECT_SET_DEFENSE_FINAL)

function Card.AdjustStats(c,...)
  local ae=c:AdjustAttack(...)
  local de=c:AdjustDefense(...)
  return ae,de
end

function Card.AdjustOrdering(c,...)
  if c:HasLevel() then
    return c:AdjustLevel(...)
  else
    return c:AdjustRank(...)
  end
end

Card.IsNotSetCard=Negate(Card.IsSetCard)

function Card.SSet(c,tp,ttp)
  ttp=ttp or tp
  return Duel.SSet(tp,c,ttp)
end

function Card.GetKind(c)
  return bit.band(c:GetType(),TYPE_MONSTER+TYPE_SPELL+TYPE_TRAP)
end

function Card.GetRelativeSequance(c,p)
  local s=c:GetSequence()
  return c:IsControler(p) and s or 4-s
end

function Card.IsRelativeSequance(c,p,s)
  s=c:IsControler(p) and s or 4-s
  return c:IsSequence(s)
end

Card.IsOffField=N(Card.IsOnField)

Card.CheckCounter=CMP(Card.GetCounter,math.less_eq(1))

function Card.IsSummonPlayer(c,p)
  return c:GetSummonPlayer()==p
end

function Card.IsPreviousControler(c,p)
  return c:GetPreviousControler()==p
end

function Card.HasFlag(c,id,n,f)
  id=id or c:GetOriginalCode()
  n=n or 0
  if not f then f=math.not_equal end
  return f(c:GetFlagEffect(id),n)
end

function Card.IsReasonCard(c,cc)
  local rc=c:GetReasonCard()
  return rc==cc
end

function Card.ChangePosition(c,au,ad,du,dd,noflip,setavailable)
  return Duel.ChangePosition(c,au,ad,du,dd,noflip,setavailable)
end

local idable=Card.IsDiscardable
function Card.IsDiscardable(c,...)
  if c:IsHasEffect(EFFECT_CANNOT_BE_DISCARD) then return false end
  return idable(c,...)
end

function Card.ReleaseRitualMaterial(c)
  return Duel.ReleaseRitualMaterial(c:AsGroup())
end

function Card.OperationInfo(c,cat,n)
  if type(c)~="Card" then return CC(Card.OperationInfo,c,n) end
  n=n or 0
  Duel.SetOperationInfo(n,cat,c,1,0,0)
end

function Card.GetControl(c,p,r,t)
  return Duel.GetControl(c,p,r,t)
end

function Card.MaxBy(c,v,...)
  local args=table.pack(...)
  local d=c
  for n=1,args.n do
    local chk=v(args[n])
    if chk>v(d) then
      d=args[n]
    end
  end
  return d
end

function Card.MinBy(c,v,...)
  local args=table.pack(...)
  local d=c
  for n=1,args.n do
    local chk=v(args[n])
    if chk<v(d) then
      d=args[n]
    end
  end
  return d
end

Card.Summon=Flip(Duel.Summon)

Card.MoveSequence=Duel.MoveSequence
