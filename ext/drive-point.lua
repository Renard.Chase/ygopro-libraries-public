local src="drive-point"
Extensions.Register(src,1,0,"Drive Point System")
Extensions.Require(src,"group",1,0)
Extensions.Require(src,"duel",1,3)
Extensions.Require(src,"rp-ch",1,0)

DP={}

local dp_id=38000000
local dp_lock=true

local function lock_dp()
  dp_lock=true
end

local function unlock_dp()
  dp_lock=false
end

local function setStatus(c)
  Effect.CreateEffect(c)
    :SetType(EFFECT_TYPE_SINGLE)
    :SetCode(EFFECT_CANNOT_TO_HAND)
    :SetProperty(EFFECT_FLAG_SET_AVAILABLE+EFFECT_FLAG_CANNOT_NEGATE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
    --:SetRange(LOCATION_MZONE)
    :SetRange(LOCATION_EXTRA)
    :SetCondition(function (e) local c=e:GetHandler() return c:IsLocation(--[[LOCATION_MZONE]]LOCATION_EXTRA) and c:IsFaceup() and dp_lock end)
    :RegisterClone(c,EFFECT_CANNOT_TO_DECK)
    :RegisterClone(c,EFFECT_CANNOT_TO_GRAVE)
    :RegisterClone(c,EFFECT_CANNOT_REMOVE)
    :RegisterClone(c,EFFECT_CANNOT_RELEASE)
    :RegisterClone(c,EFFECT_CANNOT_USE_AS_COST)
    :RegisterClone(c,EFFECT_CANNOT_BE_FUSION_MATERIAL)
    :RegisterClone(c,EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
    :RegisterClone(c,EFFECT_CANNOT_BE_XYZ_MATERIAL)
    :RegisterClone(c,EFFECT_CANNOT_SPECIAL_SUMMON)
    :RegisterClone(c,EFFECT_CANNOT_FLIP_SUMMON)
    :RegisterClone(c,EFFECT_CANNOT_SUMMON)
    :RegisterClone(c,EFFECT_CANNOT_MSET)
    :RegisterClone(c,EFFECT_CANNOT_SSET)
end

local dpf=CC(Card.IsCode,dp_id)

-- Remember to keep group alive after return if using
local function create_dp(tp,n)
  n=n or 1
  local g=Group.CreateGroup()
  for _=1,n do
    local t=Duel.CreateToken(tp,dp_id,nil,nil,nil,nil,nil,nil)
    g:AddCard(t)
  end
  Duel.SendtoExtraP(g,tp,REASON_RULE+REASON_IGNORE)
  g:LoopFor(setStatus)
  g:ConfirmCards()
  return g
end

local function destroy_dp(g)
  if type(g)=="card" then g=g:AsGroup() end
  unlock_dp()
  g:SendtoDeck(0,-2,REASON_RULE+REASON_IGNORE)
  lock_dp()
end

function DP.Add(tp,n)
  local n=n or 1
  if not _drive_point_enabled then return end
  return create_dp(tp,n)
end

function DP.Has(tp,n)
  local n=n or 1
  if not _drive_point_enabled then return false end
  return Duel.IsExistingMatchingCard(dpf,tp,LOCATION_EXTRA,nil,n,nil)
end

function DP.Count(tp)
  return Duel.GetMatchingGroupCount(dpf,tp,LOCATION_EXTRA,nil,nil)
end

function DP.Remove(tp,n)
  n=n or 1
  if not _drive_point_enabled then return false end
  local g=Duel.GetMatchingGroup(dpf,tp,LOCATION_EXTRA,0,nil):Take(n)
  destroy_dp(g)
end

DP.CanPay=DP.Has
DP.Pay=DP.Remove

-- Configures DP System for use
local function sf(c)
  return c:GetSummonLocation()~=LOCATION_EXTRA
end

local function chk(tp)
  return Duel.GetCustomActivityCount(dp_id,tp,ACTIVITY_SPSUMMON)==0
end

local function desc(e,tp,eg,ep,ev,re,r,rp)
  return ep~=rp and (re==nil or not re:GetDataOrDefault("drive",false)) and chk(rp) and (not Duel.BitCheck(r,REASON_BATTLE))
end

local function deso(e,tp,eg,ep,ev,re,r,rp)
  DP.Add(rp)
end

local function bdesf(c)
  local dc=c:GetBattleTarget()
  local tp=c:GetPreviousControler()
  local op=dc:IsLocation(LOCATION_ONFIELD) and dc:GetControler() or dc:GetPreviousControler()

  return tp~=op and chk(op)
end

local function bdesg(c)
  local dc=c:GetBattleTarget()
  return dc:IsLocation(LOCATION_ONFIELD) and dc:GetControler() or dc:GetPreviousControler()
end

local function bdesc(e,tp,eg,ep,ev,re,r,rp)
  return eg:Any(bdesf)
end

local function bdeso(e,tp,eg,ep,ev,re,r,rp)
  local g=eg:DropUnless(bdesf)
  local t=g:Map(bdesg)
  for p in table.simple_iterator(t) do
    DP.Add(p)
  end
end

local function o(e,tp,eg,ep,ev,re,r,rp)
  DP.Add(ep)
end

local function dmc(e,tp,eg,ep,ev,re,r,rp)
  return Duel.GetAttackTarget()==nil and chk(ep)
end

local function ldc(e,tp,eg,ep,ev,re,r,rp)
  return ev>=1500 and chk(ep)
end

local function tec(e,tp,eg,ep,ev,re,r,rp)
  return chk(Duel.GetTurnPlayer())
end

local function teo(e,tp,eg,ep,ev,re,r,rp)
  DP.Add(Duel.GetTurnPlayer())
end

local function dp_start(c)
  c=c or Character.GetCharacter()
  Duel.AddCustomActivityCounter(dp_id,ACTIVITY_SPSUMMON,sf)
  -- Destroy
  Effect.CreateEffect(c)
    :SetActivation(EFFECT_TYPE_CONTINUOUS_F,EVENT_DESTROY,LOCATION_ALL)
    :SetProperty(EFFECT_FLAG_COMBINATION_RULES)
    :SetCondition(desc)
    :SetOperation(deso)
    :Register(c)
  -- Battle Destroy
  Effect.CreateEffect(c)
    :SetActivation(EFFECT_TYPE_CONTINUOUS_F,EVENT_BATTLE_DESTROYED,LOCATION_ALL)
    :SetProperty(EFFECT_FLAG_COMBINATION_RULES)
    :SetCondition(bdesc)
    :SetOperation(bdeso)
    :Register(c)
  -- Direct Damage
  Effect.CreateEffect(c)
    :SetActivation(EFFECT_TYPE_CONTINUOUS_F,EVENT_BATTLE_DAMAGE,LOCATION_ALL)
    :SetProperty(EFFECT_FLAG_COMBINATION_RULES)
    :SetCondition(dmc)
    :SetOperation(o)
    :Register(c)
  -- Large Damage
  Effect.CreateEffect(c)
    :SetActivation(EFFECT_TYPE_CONTINUOUS_F,EVENT_DAMAGE,LOCATION_ALL)
    :SetProperty(EFFECT_FLAG_COMBINATION_RULES)
    :SetCondition(ldc)
    :SetOperation(o)
    :Register(c)
  -- Turn End
  Effect.CreateEffect(c)
    :SetActivation(EFFECT_TYPE_CONTINUOUS_F,EVENT_TURN_END,LOCATION_ALL,1)
    :SetProperty(EFFECT_FLAG_COMBINATION_RULES)
    :SetCondition(tec)
    :SetOperation(teo)
    :Register(c)
end

function DP.Enable(c)
  if not _drive_point_enabled then
    dp_start(c)
  end
  _drive_point_enabled=true
end

function DP.Cost(n)
  return function (e,tp,eg,ep,ev,re,r,rp,chk)
    if chk==0 then
      return DP.CanPay(tp,n)
    end
    DP.Pay(tp,n)
  end
end

function DP.CanAdd(p,n)
  return chk(p)
end
