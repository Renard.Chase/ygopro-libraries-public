-- Vertex's Scripting Library
local effectGUID=0
Extensions.Register("data-effect",1,1,"Data Effects")
Extensions.Require("data-effect","maybe",1,0)
local def={}
def.get=Effect.GetLabelObject
def.set=Effect.SetLabelObject
def.cr=Effect.CreateEffect
def.cl=Effect.Clone
def.gcr=Effect.GlobalEffect

local function GetD(e)
  return def.get(e):GetValue()()
end

local function SetD(e,d)
  def.get(e):SetValue(function () return d end)
end

function Effect.CreateEffect(c,...)
  local b,e=pcall(def.cr,c,...)
  if not b then error(e,2) end
  local d=def.cr(c)
  local data={}
  effectGUID=effectGUID+1
  d:SetCondition(function () return effectGUID end)
  d:SetValue(function () return data end)
  def.set(e,d)
  return e
end

function Effect.GlobalEffect(...)
  local e=def.gcr(...)
  local d=def.gcr()
  local data={}
  effectGUID=effectGUID+1
  d:SetCondition(function () return effectGUID end)
  d:SetValue(function () return data end)
  def.set(e,d)
  return e
end

function Effect.Clone(e,...)
  local n=def.cl(e,...)
  local d=e:GetHandler() and def.cr(e:GetHandler()) or def.gcr()
  local data=GetD(e)
  local lo=n:GetLabelObject()
  effectGUID=effectGUID+1
  d:SetCondition(function () return effectGUID end)
  d:SetValue(function () return data end)
  def.set(n,d)
  if n==nil then error ("Data Effect : Effect.Clone, resulting effect is nil",2) end
  n:SetLabelObject(lo)
  return n
end

function Effect.GetLabelObject(e,...)
  local d=def.get(e)
  return def.get(d,...)
end

function Effect.SetLabelObject(e,...)
  local d=def.get(e)
  return def.set(d,...)
end

function Effect.AddData(e,key,value)
  local data=GetD(e)
  local r=data[key]
  data[key]=value
  SetD(e,data)
  return r
end
Effect.SetData=Effect.AddData

function Effect.NewData(e,key,value)
  e:AddData(key,value)
  return e
end

function Effect.GetData(e,...)
  local args=table.pack(...)
  local data=GetD(e)
  local res={}
  for n=1,args.n do
    table.insert(res,data[args[n]])
  end
  return table.unpack(res)
end

function Effect.ClearData(e,key)
  local data=GetD(e)
  local r={}
  local x=nil
  for k,v in pairs(data) do
    if (k~=key) then
      r[k]=v
    else
      x=v
    end
  end
  SetD(e,r)
  return x
end

function Effect.HasData(e,key)
  local data=GetD(e)
  return data[key]~=nil
end

function Effect.GetDataOrDefault(e,key,default)
  return e:HasData(key) and e:GetData(key) or default
end

function Effect.MaybeGetData(e,key)
  return e:HasData(key) and Maybe.Just(e:GetData(key)) or Maybe.Nothing()
end

function Effect.EitherGetDataOrDefault(e,key,default)
  local f=function ()
    local E=require "lib.either"
    return E
  end
  local E=pcall(f)
  if not E then error("Effect.EitherGetDataOrDefault, could not find lib/either.lua",2) end
  return e:HasData(key) and Either.Left(e:GetData(key)) or Either.Right(default)
end

function Effect.GetGUID(e)
  return def.get(e):GetCondition()()
end
