-- Vertex's Scripting Library
Extensions.Register("io",1,0,"io functions extension")
function file_exists(file)
  local io=require "io"
  local f=io.open(file)
  if f then f:close() end
  return f~=nil
end
