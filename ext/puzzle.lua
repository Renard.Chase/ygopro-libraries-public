-- Vertex's Scripting Library
--Card Debug.AddCard(int code, int owner, int player, int location, int seq, int pos[, bool proc=false])
local io=require "io"

local src="puzzle"
Extensions.Register(src,1,1,"Puzzle Script Extensions")
Extensions.Require(src,"io",1,0)
Extensions.Require(src,"string",1,0)
Extensions.Require(src,"function",1,5)
Extensions.Require(src,"table",1,3)
Extensions.Require(src,"config",1,0)

Puzzle={}
local dir="puzzle/"

setmetatable(Puzzle,{type="Puzzle Duel Functions"})

local function check(a,...)
  local args=table.pack(...)
  return table.any(args,math.equal,a)
end

function Puzzle.LoadFieldDefinition(file)
  file=file or Config.GetValue("puzzle_field")
  if not file_exists(dir .. "field/" .. file) then error("Puzzle LoadFieldDefinition Error: File \"" .. dir .. "field/" .. file .. "\" does not exist.",2) end
  local p=nil
  local z=nil
  for line in io.lines(dir .. "field/" .. file) do
    line=line:trim()
    local s=line:upper()
    local c=C(check,s)
    if c("#PLAYER") then
      p=0
    elseif c("#OPPONENT") then
      p=1
    elseif c("#MZONE","#FRONT","#FRONTROW") then
      z=LOCATION_MZONE
    elseif c("#SZONE","#BACK","#BACKROW") then
      z=LOCATION_SZONE
    elseif c("#HAND") then
      z=LOCATION_HAND
    elseif c("#GRAVE","#GRAVEYARD") then
      z=LOCATION_GRAVE
    elseif c("#DECK","#MAIN","#MAINDECK") then
      z=LOCATION_DECK
    elseif c("#EXTRA","#EXTRADECK","#XDECK") then
      z=LOCATION_EXTRA
    elseif c("#BANISH","#BANISHED","#REMOVE","#REMOVED","#RFG") then
      z=LOCATION_REMOVED
    elseif line=="" then
    else
      local data=line:split(" ")
      local fz=z==LOCATION_MZONE or z==LOCATION_SZONE
      if data[2]==nil and fz then error("Puzzle LoadFieldDefinition Error: Sequence not given for field card.") end

      local faceup=data[1]:sub(1,1)=="+"
      local id=tonumber(data[1]:sub(2,data[1]:len()))
      local seq=data[2] and tonumber(data[2])-1 or Duel.GetFieldGroupCount(p,z,0)
      local bp=(data[3] and data[3]:lower()=="attack")
      
      local pos=faceup and POS_FACEUP or POS_FACEDOWN
      if fz then
        if faceup and not bp then
          pos=POS_FACEUP_DEFENSE
        elseif faceup and bp then
          pos=POS_FACEUP_ATTACK
        elseif not faceup and not bp then
          pos=POS_FACEDOWN_DEFENSE
        else
          pos=POS_FACEDOWN_ATTACK
        end
      end

      Debug.AddCard(id,p,p,z,seq,pos,false)
    end
  end
end

function Puzzle.LoadConfiguration(file,end_reload)
  end_reload=end_reload or false
  file=file or Config.GetValue("puzzle_config")
  if not file_exists(dir .. "config/" .. file) then error("Puzzle LoadConfiguration Error: File \"" .. dir .. "config/" .. file .. "\" does not exist.",2) end
  local data={}
  local d=-1
  data[0]={}
  data[1]={}
  data[2]={}
  for line in io.lines(dir .. "config/" .. file) do
    line=line:trim()
    if line:upper()=="#PLAYER" or line:upper()=="#OPPONENT" or line:upper()=="#CONFIG" then
      d=line:upper()=="#PLAYER" and 0 or (line:upper()=="#OPPONENT" and 1 or 2)
    elseif line=="" then
    else
      r=line:gsub(" ?= ?","="):split("=")
      data[d][r[1]:lower()]=r[2]
    end
  end
  local attack=data[2]["attack"] and data[2]["attack"]:lower()=="true"
  local ai=data[2]["ai"] and data[2]["ai"]:lower()=="true"
  local dbg=data[2]["debug"] and data[2]["debug"]:lower()=="true"
  mode=(attack and DUEL_ATTACK_FIRST_TURN or 0)+(ai and DUEL_SIMPLE_AI or 0)
  Debug.SetAIName(data[1]["name"] or "Opponent")
  Debug.ReloadFieldBegin(mode)
  Debug.SetPlayerInfo(0,data[0]["lp"] or 8000,data[0]["start"] or 0,data[0]["draw"] or 0)
  Debug.SetPlayerInfo(1,data[1]["lp"] or 8000,data[1]["start"] or 0,data[1]["draw"] or 0)
  if data[2]["remove_hand_limit"] and data[2]["remove_hand_limit"]:lower()=="true" then
    local e1=Effect.GlobalEffect()
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetCode(EFFECT_HAND_LIMIT)
    e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
    e1:SetTargetRange(1,1)
    e1:SetValue(100)
    Duel.RegisterEffect(e1,0)
  end
  if dbg then EnableDebugFlag() end
  if end_reload then Debug.ReloadFieldEnd() end
end

function Puzzle.Load(field,conf,enable_puzzle,end_reload)
  if end_reload==nil then end_reload=true end
  if enable_puzzle==nil then enable_puzzle=true end
  Puzzle.LoadConfiguration(conf,false)
  Puzzle.LoadFieldDefinition(field)
  if end_reload then Debug.ReloadFieldEnd() end
  if enable_puzzle then aux.BeginPuzzle() end
end

function Puzzle.LoadDef(enable_puzzle,end_reload)
  Puzzle.Load(nil,nil,enable_puzzle,end_reload)
end
