-- Vertex's Scripting Library
local src="setcodes"
Extensions.Register(src,1,1,"Set Code Library")
Extensions.Require(src,"table",1,1)
Extensions.Require(src,"card",1,1)
Extensions.Require(src,"constant",1,2)

local SET_ROOT=1
local SET_CARDS=2

local d={}

local function GetCards(id)
  local s=d[id]
  if not s then return {} end
  local cards=table.clone(s[SET_CARDS])
  local sets=s[SET_ROOT]
  for _,v in ipairs(sets) do
    table.merge_array(cards,GetCards(v),false)
  end
  return cards
end

local function CheckSetID(c,id)
  local cards=GetCards(id)
  local isCode=c:IsCode(table.unpack(cards))
  return c:IsCode(table.unpack(cards))
end

local isc=Card.IsSetCard

local function RoseFix(c)
  if isc(c,ARCHETYPE_ROSE) then return true end
  return c:IsCode(table.unpack(ARCHETYPE_ROSE_IDS))
end

function Card.IsSetCard(c,...)
  local args=table.pack(...)
  --Multi Codes
  if args.n==0 then
    error("Card.IsSetCard requires at least 2 arguments",2)
  elseif args.n>1 then
    local hasCode=false
    for n=1,args.n do
      if type(args[n])=="boolean" and args[n]==false then break end
      hasCode=c:IsSetCard(args[n])
      if hasCode then break end
    end
    return hasCode
  end
  --Single Code
  local sc=args[1]
  if type(sc)=="string" then
    return CheckSetID(c,sc)
  else
    local n=sc==ARCHETYPE_ROSE and RoseFix(c) or nil
    if type(n)=="boolean" then return n end
    return isc(c,sc)
  end
end

function Card.IsNotSetCard(...)
  return not Card.IsSetCard(...)
end

local function init(sid)
  if not d[sid] then
    d[sid]={{},{}}
  end
end

function Card.AddSetID(c,...)
  local args=table.pack(...)
  for n=1,args.n do
    init(args[n])
    cards=d[args[n]][SET_CARDS]
    if not c:IsCode(table.unpack(cards)) then
      table.insert(cards,c:GetCode())
    end
  end
end

function MapIDToSet(id,...)
  local args=table.pack(...)
  for n=1,args.n do
    init(args[n])
    cards=d[args[n]][SET_CARDS]
    local check=true
    for _,v in pairs(cards) do
      if v==id then check=false end
    end
    if check then
      table.insert(cards,id)
    end
  end
end

function AddRootArchetype(sub,root)
  init(sub)
  init(root)
  local roots=d[root][SET_ROOT]
  if table.all(roots,function (a) return a~=root end) then
    table.insert(roots,sub)
  end
end

function Card.GetSets(c,database_only)
  local sets={}
  for n=1,0xFFF do
    --if c:IsSetCard(n) then table.insert(sets) end
    if n~=0x08a then
      if c:IsSetCard(n) then table.insert(sets,n) end
      if c:IsSetCard(0x1000+n) then table.insert(sets,0x1000+n) end
      if c:IsSetCard(0x2000+n) then table.insert(sets,0x2000+n) end
      if c:IsSetCard(0x4000+n) then table.insert(sets,0x4000+n) end
      if c:IsSetCard(0x8000+n) then table.insert(sets,0x8000+n) end
    else
      for x=0,0xF do
        if c:IsSetCard((x*0x1000)+n) then table.insert(sets,(x*0x1000)+n) end
      end
    end
  end
  if not database_only then
    for _,v in ipairs(d) do
      local cards=v[SET_CARDS]
      if c:IsCode(table.unpack(cards)) then
        table.insert(sets,v)
      end
    end
  end
  return sets
end

function GetSetIDInfo()
  if CheckDebugFlag() then error("GetSetIDInfo requires EnableDebugFlag.") end
  return d
end
