local io=require "io"
local src="rp-ch"
Extensions.Register(src,1,0,"RP Characters Extension")
Extensions.Require(src,"config",1,0)
Extensions.Require(src,"io",1,0)
Extensions.Require(src,"string",1,0)
Extensions.Require(src,"table",1,4)

Config.LoadFile("rp")
local enabled=Config.GetBooleanOrDefault("rp-enabled",false)
local setcode=Config.MaybeGetInteger("rp-chars-setcode")
local confirm=Config.GetBooleanOrDefault("rp-confirm",true)

Character={}

local code=setcode:GetValueOrDefault(0)
local icode=code+0x1000
Character.code=code

local filter={icode,OPCODE_ISSETCARD}
local cfilter=CC(Card.IsSetCard,code)

-- Must happen before script terminates to ensure that character cards are removed if not used
if not enabled then 
  if setcode:HasValue() then
    local rge=Card.RegisterEffect
    local flag=true
    Card.RegisterEffect=function (c,...)
      if flag then
        flag=false
        Effect.CreateEffect(c)
          :SetActivation(EFFECT_TYPE_CONTINUOUS_F,EVENT_PHASE_START+PHASE_DRAW,LOCATION_ALL)
          :SetProperty(EFFECT_FLAG_COMBINATION_RULES)
          :SetOperation(function (e,tp,eg,ep,ev,re,r,rp)
            local g=Duel.GetMatchingGroup(cfilter,nil,LOCATION_ALL)
            g:LoopFor(function (tc)
              local check=-1
              if tc:IsLocation(LOCATION_HAND) then
                check=tc:GetControler()
              end
              tc:SendtoDeck(0,-2,REASON_RULE+REASON_IGNORE)
              if check>-1 then Duel.Draw(check,1,REASON_RULE+REASON_IGNORE) end
            end)
            g:DeleteGroup()
            e:Reset()
          end)
          :Register(c)
      end
      return rge(c,...)
    end
  end
  return
end
--if not file_exists("rp/char-list.txt") then return end
if setcode:IsNothing() then return end

-- Main Section
_lock_characters=true
EVENT_CUSTOM_RP_INIT=EVENT_CUSTOM+Config.GetIntegerOrDefault("rp-init-event-id",99999999)

--[[local ch={}
local filter={}
local n=-1
for line in io.lines("rp/char-list.txt") do
  line=line:trim()
  table.insert(ch,line)
  n=n+1
end
if n<0 then return end

for c in table.simple_iterator(ch) do
  table.insert(filter,c)
  table.insert(filter,OPCODE_ISCODE)
end

for o=0,n do
  table.insert(filter,OPCODE_OR)
end

--[[for _,v in ipairs(filter) do
  Debug.Message(v)
end]]


local get=function (p)
  local chk=Duel.PlayerFilterCheck(cfilter,p,LOCATION_ALL)
  if chk then
    local c=Duel.GetFirstMatchingCard(cfilter,p,LOCATION_ALL,0,nil)
    return p,c:GetCode(),c
  else
    return p,Duel.AnnounceCardFilter(p,table.unpack(filter)),nil
  end
end

local exec=function (p,id,exist)
  local t=exist or Duel.CreateToken(p,id,nil,nil,nil,nil,nil,nil)
  if t:IsSetCard(code) then
    Duel.SendtoExtraP(t,p,REASON_RULE)
    if t:IsPreviousLocation(LOCATION_HAND) then
      Duel.Draw(p,1,REASON_RULE+REASON_IGNORE)
    end
    Effect.CreateEffect(t)
      :SetType(EFFECT_TYPE_SINGLE)
      :SetCode(EFFECT_CANNOT_TO_HAND)
      :SetProperty(EFFECT_FLAG_SET_AVAILABLE+EFFECT_FLAG_CANNOT_NEGATE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
      --:SetRange(LOCATION_MZONE)
      :SetRange(LOCATION_EXTRA)
      :SetCondition(function (e) local c=e:GetHandler() return c:IsLocation(--[[LOCATION_MZONE]]LOCATION_EXTRA) and c:IsFaceup() and _lock_characters end)
      :RegisterClone(t,EFFECT_CANNOT_TO_DECK)
      :RegisterClone(t,EFFECT_CANNOT_TO_GRAVE)
      :RegisterClone(t,EFFECT_CANNOT_REMOVE)
      :RegisterClone(t,EFFECT_CANNOT_RELEASE)
      :RegisterClone(t,EFFECT_CANNOT_USE_AS_COST)
      :RegisterClone(t,EFFECT_CANNOT_BE_FUSION_MATERIAL)
      :RegisterClone(t,EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
      :RegisterClone(t,EFFECT_CANNOT_BE_XYZ_MATERIAL)
      :RegisterClone(t,EFFECT_CANNOT_SPECIAL_SUMMON)
      :RegisterClone(t,EFFECT_CANNOT_FLIP_SUMMON)
      :RegisterClone(t,EFFECT_CANNOT_SUMMON)
      :RegisterClone(t,EFFECT_CANNOT_MSET)
      :RegisterClone(t,EFFECT_CANNOT_SSET)
  end
  return t
end

local function op(e,tp,eg,ep,ev,re,r,rp)
  local g=Group.CreateGroup()
  g:AddCard(exec(get(0)))
  g:AddCard(exec(get(1)))
  e:Reset()
  if confirm then g:ConfirmCards() end
  Duel.RaiseEvent(Group.CreateGroup(),EVENT_CUSTOM_RP_INIT,e,REASON_RULE,0,0,0)
end

local function clim(e,rp,tp)
  local c=e:GetHandler()
  return c:IsSetCard(code)
end

local function chainop(e,tp,eg,ep,ev,re,r,rp)
  local c=re:GetHandler()
  if c:IsSetCard(code) then
    Duel.SetChainLimit(clim)
  end
end

local ce=Effect.CreateEffect
function Effect.CreateEffect(c,...)
  if enabled then
    Effect.GlobalEffect()
      :SetActivation(EFFECT_TYPE_CONTINUOUS_F,EVENT_CHAINING)
      :SetOperation(chainop)
      :RegisterDuel(0)

    Effect.GlobalEffect()
      :SetActivation(EFFECT_TYPE_CONTINUOUS_F,EVENT_PREDRAW,nil,1)
      :SetProperty(NO_COPY_NO_DISABLE+EFFECT_FLAG_CANNOT_NEGATE+EFFECT_FLAG_IMMEDIATELY_APPLY+EFFECT_FLAG_IGNORE_IMMUNE)
      :SetOperation(op)
      :RegisterDuel(0)

    enabled=false
  end
  return ce(c,...)
end

function Character.GetCharacter(tp)
  return Duel.GetFirstMatchingCard(Card.IsSetCard,tp,LOCATION_EXTRA,nil,nil,code)
end
