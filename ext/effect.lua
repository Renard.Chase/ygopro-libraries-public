-- Vertex's Scripting Library
Extensions.Register("effect",1,3,"Effect")
Extensions.Require("effect","type",1,0)
function Effect.SetUncopyable(e)
  return e:SetProperty(e:GetProperty()+EFFECT_FLAG_UNCOPYABLE)
end

function Effect.SetNoDisable(e)
  return e:SetProperty(e:GetProperty()+EFFECT_FLAG_CANNOT_DISABLE)
end

function Effect.SetNoCopyNoDisable(e)
  e:SetUncopyable()
  return e:SetNoDisable()
end

function Effect.ResetTurnEnd(e)
  return e:SetReset(RESET_PHASE+PHASE_END)
end

function Effect.ResetLeave(e)
  return e:SetReset(RESET_EVENT+0x1fe0000)
end

function Effect.ResetEndOrLeave(e)
  return e:SetReset(RESET_PHASE+PHASE_END+RESET_EVENT+0x1fe0000)
end

function Effect.SetEffectDescription(e,id,n)
  return e:SetDescription(aux.Stringid(id,n))
end

local ce=Effect.CreateEffect
function Effect.CreateEffect(c,n,id,...)
  local b,e=pcall(ce,c,n,id,...)
  if not b then error(e,2) end
  id=id or c:GetOriginalCode()
  if n then e:SetEffectDescription(id,n) end
  return e
end

function Effect.CreateSingleEffect(c,co,pr,va,ra,re,rg)
  if type(co)=="table" then
    local t=co
    co=t.code
    pr=t.property
    va=t.value
    ra=t.range
    re=t.reset
    rg=t.register
  end
  if ra==nil then ra=LOCATION_MZONE end
  pr=pr or 0
  re=re or 0
  local e=Effect.CreateEffect(c)
  e:SetType(EFFECT_TYPE_SINGLE)
  e:SetCode(co)
  if ra then e:SetRange(ra) end
  e:SetProperty(pr)
  e:SetReset(re)
  e:SetValue(va)
  if rg then e:Register(type(rg)=="card" and rg or c) end
  return e
end

function Effect.MultiSummonEffect(e,normal,flip,special,register,c)
  local e1,e2,e3=nil
  local f=function (b,s) if b then local e1=e:Clone() e1:SetCode(s) return e1 end end
  e1=f(normal,EVENT_SUMMON_SUCCESS)
  e2=f(flip,EVENT_FLIP_SUMMON_SUCCESS)
  e3=f(special,EVENT_SPSUMMON_SUCCESS)
  if register then c:RegisterEffects(e1,e2,e3) end
  return e1,e2,e3
end

function Card.AddActivateProc(c,...)
  local e=Effect.CreateEffect(c,...)
  e:SetType(EFFECT_TYPE_ACTIVATE)
  e:SetCode(EVENT_FREE_CHAIN)
  c:RegisterEffect(e)
  return e
end

function Effect.IsRelateToHandler(e)
  return e:GetHandler():IsRelateToEffect(e)
end
Effect.IsNotRelateToHandler=Negate(Effect.IsRelateToHandler)

function Effect.Register(e,c,...)
  if c==nil then return function(x,...) e:Register(x,...) end end
  c:RegisterEffect(e,...)
  return e
end

function Effect.RegisterDuel(e,...)
  Duel.RegisterEffect(e,...)
  return e
end

local clone=Effect.Clone
function Effect.Clone(e,code,value)
  local c=clone(e)
  if code~=nil then c:SetCode(code) end
  if value~=nil then c:SetValue(value) end
  return c
end

function Effect.RegisterClone(e,c,...)
  local args=table.pack(...)
  if c==nil then return function(x,...) local a=table.pack(...) e:RegisterClone(x,table.unpack(a)) end end
  local cl=e:Clone(table.unpack(args))
  c:RegisterEffect(cl)
  return cl
end

function Effect.IsRelatedTo(e,...)
  local g=Group.FromCards(...)
  return g:All(Card.IsRelateToEffect,e)
end

function Effect.IsNotRelatedTo(...)
  return not Effect.IsRelatedTo(...)
end

function Effect.SetSumParam(e)
  return e:SetProperty(EFFECT_FLAG_SPSUM_PARAM+(e:GetProperty() or 0))
end

function Card.AddSummonProc(c,range,n,id)
  range=range or LOCATION_HAND
  local e=Effect.CreateEffect(c,n,id)
  e:SetType(EFFECT_TYPE_FIELD)
  e:SetCode(EFFECT_SPSUMMON_PROC)
  e:SetUncopyable()
  e:SetRange(range)
  c:RegisterEffect(e)
  return e
end

function Card.AddHandTrapActivation(c,con,cost)
  con=con or aux.TRUE
  local e=Effect.CreateEffect(c)
  e:SetType(EFFECT_TYPE_SINGLE)
  e:SetCode(EFFECT_TRAP_ACT_IN_HAND)
  e:SetCondition(con)
  if cost then e:SetCost(cost) end
  c:RegisterEffect(e)
  return e
end

function Duel.GraveBanishCost(e,tp,eg,ep,ev,re,r,rp,chk)
  local c=e:GetHandler()
  if chk==0 then
    local canBanish=c:IsAbleToRemoveAsCost()
    return canBanish
  end
  c:BanishAsCost()
end

function Card.CreateGraveBanishEffect(c,pr,code,ty,n,id)
  ty=ty or (c:IsMonster() and EFFECT_TYPE_IGNITION or EFFECT_TYPE_QUICK_O)
  code=code or EVENT_FREE_CHAIN

  local e=Effect.CreateEffect(c,n,id)
  e:SetProperty(pr or 0)
  e:SetType(ty)
  e:SetCode(code)
  e:SetRange(LOCATION_GRAVE)
  e:SetCost(Duel.GraveBanishCost)
  return e
end

function Effect.AddProperty(e,prop)
  return e:SetProperty((e:GetProperty() or 0)+prop)
end

function Effect.SetAllowDelay(e)
  return e:AddProperty(EFFECT_FLAG_DELAY)
end

function Effect.SetTargeting(e)
  return e:AddProperty(EFFECT_FLAG_CARD_TARGET)
end

function Effect.SetDamageStep(e,step,calc)
  if step~=false then e:AddProperty(EFFECT_FLAG_DAMAGE_STEP) end
  if calc then e:AddProperty(EFFECT_FLAG_DAMAGE_CAL) end
  return e
end

function Effect.NewEffect(c,data,def_ra)
  local n,id=-1,-1
  data.desc=data.desc or data.ds or data.d
  if data.desc~=nil then
    if type(data.desc)=="table" then
      n=data.desc[1]
      id=data.desc[2]
    else
      n=data.desc
      id=c:GetOriginalCode()
    end
  end
  local cat=data.category or data.cat or 0
  local ty=data.ty or (c:IsMonster() and EFFECT_TYPE_IGNITION or EFFECT_TYPE_ACTIVATE)
  local co=data.code or data.co or EVENT_FREE_CHAIN
  local pr=data.property or data.pr or 0
  local ra=-1
  if def_ra or def_ra==nil then
    ra=data.range or data.ra or (c:IsMonster() and LOCATION_MZONE or LOCATION_HAND)
  else
    ra=nil
  end
  local abr=data.absolute_range or data.absolute or data.abr or data.ar
  local cl=data.count_limit or data.count or data.limit or data.cl
  local re=data.reset or data.re
  local lb=data.label or data.lb or data.l
  local lbo=data.label_object or data.object or data.lbo or data.obj or data.lo or data.o
  local ht=data.hint_timing or data.hint or data.timing or data.ht or data.h
  local cn=data.condition or data.con or data.cn or data.c
  local cs=data.cost or data.cst or data.cs
  local tg=data.target or data.tg or data.t
  local tr=data.target_range or data.tgr or data.tr
  local va=data.value or data.val or data.va or data.v
  local op=data.operation or data.op or data.o
  local ow=data.owner_player or data.owner or data.ow
  local rg=data.register or data.rg or data.r or (data.register==nil)

  local e1=Effect.CreateEffect(c)
  if n then e1:SetDescription(aux.Stringid(id,n)) end
  e1:SetCategory(cat)
  e1:SetType(ty)
  e1:SetCode(co)
  e1:SetProperty(pr)
  if ra then e1:SetRange(ra) end
  if abr then e1:SetAbsoluteRange(table.unpack(abr)) end
  if cl then e1:SetCountLimit(type(cl)=="number" and cl or table.unpack(cl)) end
  if re then e1:SetReset(re) end
  if lb then e1:SetLabel(lb) end
  if lbo then e1:SetLabelObject(lbo) end
  if ht then e1:SetHintTiming(type(ht)=="number" and ht or table.unpack(ht)) end
  if cn then e1:SetCondition(cn) end
  if cs then e1:SetCost(cs) end
  if tg then e1:SetTarget(tg) end
  if tr then e1:SetTargetRange(table.unpack(tr)) end
  if va then e1:SetValue(va) end
  if op then e1:SetOperation(op) end
  if ow then e1:SetOwnerPlayer(ow) end
  if rg then
    if type(rg)=="card" then
      rg:RegisterEffect(e1)
    elseif type(rg)=="boolean" then
      c:RegisterEffect(e1)
    end
  end
  return e1
end

function Effect.SetActivation(e,t,c,r,...)
  if t then e:SetType(t) end
  if c then e:SetCode(c) end
  if r then e:SetRange(r) end
  if ... then e:SetCountLimit(...) end
  return e
end

function Effect.SetFilterTarget(e,f,p,l,mn,mx,h,oinfo,...)
  local as=true
  if type(f)=="table" then f,as=f[1],f[2] end
  if type(p)=="function" then Debug.Message("SetTargetFilter Warning: In script \"" .. GetID(4) .. "\", player comes before function.") end
  local args=table.pack(...)
  e:SetTarget(function (e,tp,eg,ep,ev,re,r,rp,chk,chkc)
    local tgp=nil
    if p~=nil then tgp=p and tp or 1-tp end
    if chkc then
      if mn>1 then return false end
      return chkc:TargetFilterCheck(f,tgp,l,table.unpack(args)) and (as or chkc~=e:GetHandler())
    end
    local s,o=l,0
    if p==nil then o=s end
    if chk==0 then
      local ex=nil
      if not as then ex=e:GetHandler() end
      return Duel.IsExistingTarget(f,tgp,s,o,mn,ex,table.unpack(args))
    end
    Duel.HintSM(tp,h)
    local g=Duel.SelectTarget(tp,f,tgp,s,o,mn,mx,nil,table.unpack(args))
    if not oinfo then return end
    if type(oinfo)=="number" then Duel.SetOperationInfo(0,oinfo,g,g:GetCount(),0,0) return end
    oinfo(e,tp,g)
  end)
  return e
end

function Effect.SetFilterCost(e,a,f,l,mn,mx,h,as,...)
  local args=table.pack(...)
  e:SetCost(function (e,tp,eg,ep,ev,re,r,rp,chk)
    local c=e:GetHandler()
    local ex=c
    if as then ex=nil end
    if chk==0 then
      return Duel.IsExistingMatchingCard(f,tp,l,0,mn,ex,table.unpack(args))
    end
    Duel.HintSM(tp,h)
    if mn==1 and mx==1 then
      Duel.WithSingleCard(a,f,tp,l,0,ex,table.unpack(args))
      return
    end
    Duel.WithSelectedCard(a,f,tp,l,0,mn,mx,ex,table.unpack(args))
  end)
  return e
end

local function cont_activate_target(o,id,con)
  return function(e,tp,eg,ep,ev,re,r,rp)
    local c=e:GetHandler()
    local cd=o:GetCondition()
    local cs=o:GetCost()
    local tg=o:GetTarget()
    
    local flag=true
    if flag and cd then flag=flag and cd(e,tp,eg,ep,ev,re,r,rp) end
    if flag and con then flag=flag and con(e,tp,eg,ep,ev,re,r,rp) end
    if flag and cs then flag=flag and cs(e,tp,eg,ep,ev,re,r,rp,0) end
    if flag and tg then flag=flag and tg(e,tp,eg,ep,ev,re,r,rp,0) end
    if flag and id then flag=flag and not c:HasFlag(id) end
    flag=flag and Duel.SelectYesNo(tp,94)

    if flag then
      e:SetProperty(o:GetProperty())
      e:SetCategory(o:GetCategory())
      e:SetData("cont-flag",1)
      if cs then cs(e,tp,eg,ep,ev,re,r,rp,1) end
      if tg then tg(e,tp,eg,ep,ev,re,r,rp,1) end
      if id then c:RegisterFlagEffect(id,RESET_END_STANDARD,EFFECT_FLAG_OATH,1) end
      return
    end
    e:SetProperty(0)
    e:SetCategory(0)
    e:SetData("cont-flag",0)
  end
end

local function cont_activate_operation(o)
  return function (e,tp,eg,ep,ev,re,r,rp)
    if e:GetDataOrDefault("cont-flag",0)~=1 then return end
    local op=o:GetOperation()
    if op then op(e,tp,eg,ep,ev,re,r,rp) end
  end
end

function Card.AddContinuousActivateProc(c,e,id,con)
  local x=c:AddActivateProc()
  x:SetType(EFFECT_TYPE_ACTIVATE)
  x:SetCode(EVENT_FREE_CHAIN)
  x:SetTarget(CreateCheckedFunction(cont_activate_target(e,id,con)))
  x:SetOperation(cont_activate_operation(e))
  c:RegisterEffect(x)
  return x
end

function ContinuousActivateCost(id)
  return function(e,tp,eg,ep,ev,re,r,rp,chk)
    if chk~=0 then return end
    local c=e:GetHandler()
    return not c:HasFlag(id)
  end
end

local countlimit=Effect.SetCountLimit
function Effect.SetCountLimit(e,n,id,flg)
  flg=flg or 0
  if id~=nil then id=id+flg end
  return countlimit(e,n,id)
end

function Effect.SetChainLimit(e,f)
  local tg=e:GetTarget()
  return e:SetTarget(MOF(tg,CCF(function () Duel.SetChainLimit(f) end)))
end

function Effect.ResetOnCondition(e,con,re)
  Effect.CreateEffect(e:GetHandler())
    :SetActivation(EFFECT_TYPE_CONTINUOUS_F,EVENT_ADJUST)
    :SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IMMEDIATELY_APPLY)
    :SetReset(re or 0)
    :SetCondition(con)
    :SetOperation(DCF(Effect.Reset,e,WH(Effect.Reset)))
    :RegisterDuel(e:GetHandlerPlayer())
end

local est=Effect.SetType
function Effect.SetType(e,...)
  local args=table.pack(...)
  if args.n>1 and not _disable_set_type_warning then
    local c=e:GetHandler()
    Debug.Message("WARNING: Effect.SetType used with multiple paramters. Did you mean Effect.SetActivation instead?")
    if c then
      Debug.Message(c:GetCode())
    end
  end
  return est(e,...)
end

function Effect.IsHandlerPlayer(e,tp)
  return e:GetHandlerPlayer()==tp
end
