-- Vertex's Scripting Library
Extensions.Register("debug",2,0,"Debug")
Extensions.Require("debug","ext",1,0)
Extensions.SetMinVersion("debug",2)

function StartWith(c)
  if CheckDebugFlag() then error("Attempted to call debug function \"StartWith\".",2) end
  local e1=Effect.CreateEffect(c)
  e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
  e1:SetCode(EVENT_PREDRAW)
  e1:SetRange(0xFF)
  e1:SetCountLimit(1)
  e1:SetOperation(function (x) c:SendtoHand() x:Reset() end)
  Duel.RegisterEffect(e1,0)
end

function AddEvalEffect(c,n)
  n=n or 0
  if CheckDebugFlag() then error("Attempted to call debug function \"AddEvalEffect\".",2) end
  local t=Effect.CreateEffect(c)
  t:SetDescription(aux.Stringid(n,15))
  t:SetType(EFFECT_TYPE_QUICK_O)
  t:SetCode(EVENT_FREE_CHAIN)
  t:AddData("n",0)
  t:SetRange(0xFF)
  t:SetCost(function (e,tp,eg,ep,ev,re,r,rp,chk) if chk==0 then return true end e:AddData("n",e:GetData("n")+1) end)
  t:SetTarget(function (e,tp,eg,ep,ev,re,r,rp,chk) if chk==0 then return true end Duel.SetTargetParam(e:GetData("n")) e:AddData(e:GetData("n"),dofile("eval.lua")) end)
  t:SetOperation(function (e,tp,eg,ep,ev,re,r,rp) e:GetData(Duel.GetChainInfo(0,CHAININFO_TARGET_PARAM))(e,tp,eg,ep,ev,re,r,rp) end)
  c:RegisterEffect(t)
end

function AddStartOperation(c,f)
  if CheckDebugFlag() then Debug.Message("Error : Attempted to call debug function \"AddStartOperation\"") return end
  local x=Effect.CreateEffect(c)
  x:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
  x:SetCode(EVENT_PHASE_START+PHASE_STANDBY)
  x:SetRange(0xFF)
  x:SetCountLimit(1)
  x:SetOperation(function (e,tp,eg,ep,ev,re,r,rp) f(e,e:GetHandler(),e:GetOwner()) x:Reset() end)
  Duel.RegisterEffect(x,0)
end

function Dynamic(label,file)
  if CheckDebugFlag() then error("Attempted to call debug function \"Dynamic\".",2) end
  local file=file or GetID(3)
  if type(file)=="number" then
    file=table.concat({"script/c",file,".lua"})
  end
  return function(...)
    local dynamic=dofile(file)
    return dynamic[label](...)
  end
end

--[[local function setfun(f)
  return function (e,s,...)
    if CheckDebugFlag() then f(e,s,...) end
    if type(s)=="string" then
      local c=e:GetHandler()
      local code=c:GetOriginalCode()
      local path=table.concat({"script/c",code,".lua"})
      return f(e,Dynamic(s,path),...)
    else
      return f(e,s,...)
    end
  end
end

local setcon=Effect.SetCondition
local setcost=Effect.SetCost
local settg=Effect.SetTarget
local setval=Effect.SetValue
local setop=Effect.SetOperation

Effect.SetCondition=setfun(setcon)
Effect.SetCost=setfun(setcost)
Effect.SetTarget=setfun(settg)
Effect.SetValue=setfun(setval)
Effect.SetOperation=setfun(setop)]]
