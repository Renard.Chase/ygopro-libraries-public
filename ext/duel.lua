-- Vertex's Scripting Library
local src="duel"
Extensions.Register(src,1,3,"Duel")
Extensions.Require(src,"group",1,0)
Extensions.Require(src,"type",1,0)
Extensions.Require(src,"io",1,0)
Extensions.Require(src,"table",1,3)
Extensions.Require(src,"maybe",1,0)
Extensions.Require(src,"config",1,0)

local function getExcept(x) local t=type(x) if t=="nothing" then return nil elseif t:sub(1,4)=="just" then return x:GetValue() else return x or nil end end
local smc=Duel.SelectMatchingCard
function Duel.SelectMatchingCard(stp,f,tp,s,o,min,max,ex,...)
  if type(stp)=="function" then return Duel.SelectMatchingCard(f,stp,f,tp,s,o,min,max,ex,...) end
  if tp==nil then o=o or s
    else o=o or 0 end
  tp=tp or 0
  min=min or 1
  max=max or min
  if max==-1 then max=Duel.GetMatchingGroupCount(f,tp,s,o,ex,...) end
  ex=getExcept(ex)
  return smc(stp,f,tp,s,o,min,max,ex,...)
end

function Duel.SelectSingleCard(stp,f,tp,s,o,ex,...)
  if type(stp)=="function" then return Duel.SelectSingleCard(f,stp,f,tp,s,o,ex,...) end
  return Duel.SelectMatchingCard(stp,f,tp,s,o,1,1,ex,...):GetFirst()
end

function Duel.MaybeSelectSingleCard(...)
  local tc=Duel.SelectSingleCard(...)
  return tc and Maybe.Just(tc) or Maybe.Nothing()
end

local smt=Duel.SelectTarget
function Duel.SelectTarget(sp,f,p,s,o,min,max,x,...)
  if type(sp)=="function" then return Duel.SelectTarget(f,sp,f,p,s,o,min,max,x,...) end
  if p==nil then o=o or s
    else o=o or 0 end
  p=p or 0
  min=min or 1
  max=max or min
  ex=getExcept(ex)
  return smt(sp,f,p,s,o,min,max,x,...)
end

function Duel.SelectSingleTarget(sp,f,p,s,o,x,...)
  if type(sp)=="function" then return Duel.SelectSingleTarget(f,sp,f,p,s,o,x,...) end
  return Duel.SelectTarget(sp,f,p,s,o,1,1,x,...):GetFirst()
end

function Duel.MaybeSelectSingleTarget(...)
  local tc=Duel.SelectSingleTarget(...)
  return tc and Maybe.Just(tc) or Maybe.Nothing()
end

function Duel.WithSelectedCard(op,...)
  local g=Duel.SelectMatchingCard(...)
  if g:HasCard() then
    return op(g)
  else
    return nil
  end
end

function Duel.ForSelectedCard(f,...)
  local g=Duel.SelectMatchingCard(...)
  g:ForEach(f)
end

function Duel.WithSingleCard(f,...)
  local tc=Duel.SelectSingleCard(...)
  if tc then
    return f(tc)
  else
    return nil
  end
end

function Duel.MaybeSelectMatchingCard(...)
  local g=Duel.SelectMatchingCard(...)
  return g and g:HasCard() and Maybe.Just(g) or Maybe.Nothing()
end

function Duel.MaybeWithSelectedCard(op,stp,f,tp,s,o,min,max,ex,...)
  local g=Duel.SelectMatchingCard(stp,f,tp,s,o,min,max,ex,...)
  if g:HasCard() then
    return Maybe.Just(op(g))
  else
    return Maybe.Nothing()
  end
end

local dmg=Duel.Damage
function Duel.Damage(p,v,r)
  if not v then return C(Duel.Damage,p) end
  r=r or REASON_EFFECT
  return dmg(p,v,r)
end

local dd=Duel.DiscardDeck
function Duel.DiscardDeck(p,v,r)
  local r=r or REASON_EFFECT
  return dd(p,v,r)
end

local glc=Duel.GetLocationCount
function Duel.GetLocationCount(tp,l,...)
  l=l or LOCATION_MZONE
  return glc(tp,l,...)
end

function Duel.ZoneCheck(tp,loc,count)
  loc=loc or LOCATION_MZONE
  count=count or 1
  return Duel.GetLocationCount(tp,loc)>=count
end

local gmg=Duel.GetMatchingGroup
function Duel.GetMatchingGroup(f,p,s,o,e,...)
  if p==nil then o=o or s
    else o=o or 0 end
  p=p or 0
  e=getExcept(e)
  return gmg(f,p,s,o,e,...)
end

local gmgc=Duel.GetMatchingGroupCount
function Duel.GetMatchingGroupCount(f,p,s,o,e,...)
  if p==nil then o=o or s
    else o=o or 0 end
  p=p or 0
  e=getExcept(e)
  return gmgc(f,p,s,o,e,...)
end

function Duel.WithMatchingGroup(op,...)
  local g=Duel.GetMatchingGroup(...)
  return op(g)
end

function Duel.ForMatchingGroup(op,...)
  local g=Duel.GetMatchingGroup(...)
  g:ForEach(op)
end

local iemc=Duel.IsExistingMatchingCard
function Duel.IsExistingMatchingCard(f,p,s,o,c,e,...)
  if p==nil then o=o or s
    else o=o or 0 end
  c=c or 1
  e=getExcept(e)
  return iemc(f,p,s,o,c,e,...)
end

local iet=Duel.IsExistingTarget
function Duel.IsExistingTarget(f,p,s,o,c,x,...)
  s=s or LOCATION_ONFIELD
  o=o or (p and 0 or s)
  p=p or 0
  c=c or 1
  x=getExcept(x)
  return iet(f,p,s,o,c,x,...)
end

function Duel.PlayerFilterCheck(filter,tp,location,ex,...)
  location=location or LOCATION_ONFIELD
  local loc=tp and 0 or location
  tp=tp or 0
  return Duel.IsExistingMatchingCard(filter,tp,location,loc,1,ex,...)
end
Duel.PlayerFilterUncheck=N(Duel.PlayerFilterCheck)

function Duel.PlayerFilterTargetCheck(filter,tp,location,ex,...)
  location=location or LOCATION_ONFIELD
  local loc=tp and 0 or location
  tp=tp or 0
  local hasTarget=Duel.IsExistingTarget(filter,tp,location,loc,1,ex,...)
  return hasTarget
end

function Duel.GetAttackCards()
  return Duel.GetAttacker(), Duel.GetAttackTarget()
end

function Duel.GetAttackGroup()
  local g=Group.CreateGroup()
  g:AddCards(Duel.GetAttackCards())
  return g
end

local function retcon(p)
  return function(e,tp,eg,ep,ev,re,r,rp)
    local check1=p and p==Duel.GetTurnPlayer()
    local check2=not p
    return check1 or check2
  end
end

local function retop(rpos,id)
  return function(e,tp,eg,ep,ev,re,r,rp)
    local t=e:GetLabel()
    if t>0 then
      e:SetLabel(t-1)
    else
      local c=e:GetLabelObject()
      if c:GetFlagEffect(id)~=0 then
        local p=e:GetData("p")
        if e:GetData("check") and Duel.ZoneCheck(p,LOCATION_SZONE) then
          Duel.MoveToField(c,e:GetData("mp"),p,LOCATION_SZONE,POS_FACEDOWN,true)
        else
          Duel.ReturnToField(c,rpos)
        end
      end
      c:ResetFlagEffect(id)
    end
  end
end

function Duel.TempRemove(e,c,pos,r,id,tp,ph,tc,rpos,pc)
  if not type(c)=="card" then error("TempRemove second argument is not \"card\".",2) end
  r=r or REASON_EFFECT
  ph=ph or PHASE_END
  tc=tc or 1
  local rp=0
  if tp then
    rp=tp==e:GetHandlerPlayer() and RESET_SELF_TURN or RESET_OPPO_TURN
  end
  rpos=rpos or c:GetPosition()

  local check=(c:IsType(TYPE_SPELL) or c:IsType(TYPE_TRAP)) and c:IsFacedown() and c:IsLocation(LOCATION_SZONE)
  local ctl=c:GetControler()

  local fn=function () return tp==nil or Duel.TurnCheck(tp) end
  if pc==1 then
    if Duel.PhaseCheck(ph) and fn() then tc=tc+1 end
  elseif pc==2 then
    if ph>=Duel.GetCurrentPhase() and fn() then tc=tc+1 end
  end
  
  local rt=Duel.Remove(c,pos or c:GetPosition(),r+REASON_TEMPORARY)
  if rt~=0 then
    c:RegisterFlagEffect(id,RESET_EVENT+RESET_TOGRAVE+RESET_TOHAND+RESET_TODECK+RESET_TOFIELD+RESET_OVERLAY,0,1)
    local e1=Effect.CreateEffect(e:GetHandler())
    e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
    e1:SetCode(EVENT_PHASE+ph)
    e1:SetReset(RESET_PHASE+ph+rp,tc)
    e1:SetCountLimit(1)
    e1:SetLabel(tc-1)
    e1:SetLabelObject(c)
    e1:AddData("check",check)
    e1:AddData("p",ctl)
    e1:AddData("mp",e:GetHandlerPlayer())
    e1:SetCondition(retcon(tp))
    e1:SetOperation(retop(rpos,id))
    Duel.RegisterEffect(e1,e:GetHandlerPlayer())
  end
  return rt>0
end

local function makeTargetFunc(f)
  return function(...)
    local t,h=f(...)
    return h and Maybe.Just(t) or Maybe.Nothing()
  end
end

function Duel.GetUncheckedTargets(e)
  local tg=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
  if not tg then return Group.CreateGroup(),false end
  tg=tg:Filter(Card.IsRelateToEffect,nil,e)
  return tg, tg:HasCard()
end

function Duel.GetCheckedTargets(e,f,...)
  local tg,ht=Duel.GetUncheckedTargets(e)
  if not ht then return tg,ht end
  tg:RemoveUnless(f,...)
  return tg,tg:HasCard()
end

function Duel.SingleCheckedTarget(e,func,...)
  local g,c=Duel.GetCheckedTargets(e,func,...)
  local r=g:HasCard()
  g=g:GetFirst() or nil
  return g,r
end

function Duel.SingleUncheckedTarget(e)
  local g,c=Duel.GetUncheckedTargets(e)
  local r=g:HasCard()
  local g=g:GetFirst() or nil
  return g,r
end

function Duel.PlayerCheckedTargets(e,tp,f,...)
  local tg,ht=Duel.GetCheckedTargets(e,f,...)
  if not ht then return tg,ht end
  tg:RemoveUnless(Card.IsControler,tp)
  ht=tg:HasCard()
  return tg,ht
end

function Duel.PlayerSingleTarget(...)
  local g,c=Duel.PlayerCheckedTargets(...)
  return g:GetFirst(),c
end

Duel.MaybeUncheckedTarget=makeTargetFunc(Duel.SingleUncheckedTarget)
Duel.MaybeCheckedTarget=makeTargetFunc(Duel.SingleCheckedTarget)
Duel.MaybePlayerTarget=makeTargetFunc(Duel.PlayerSingleTarget)

function Duel.BitCheck(b,c,f)
  f=f or bit.band
  return f(b,c)==c
end

local dr=Duel.Draw
function Duel.Draw(p,c,r)
  c=c or 1
  r=r or REASON_EFFECT
  return dr(p,c,r)
end

function Duel.GetCounterID(counter)
  local io=require "io"
  local file="strings.conf"
  --if file_exists("expansions/live/strings.conf") then file="expansions/live/strings.conf" end
  for line in io.lines(file) do
    local t=line:split(" ")
    if t[1]=="!counter" then
      local data=t[3]:split(string.char(0xc2)..string.char(160))
      local str=""
      for k,v in ipairs(data) do
        if str=="" then str=v
          else str=str.." "..v end
      end
      if str==counter then
        return Maybe.Just(tonumber(t[2]:sub(3),16))
      end
    end
  end
  return Maybe.Nothing()
end

function Duel.CreateSummonFilter(e,tp,ty,pos,check,lim,sp)
  ty=ty or 0
  pos=pos or POS_FACEUP
  sp=sp or tp
  return function (c)
    return c:IsCanBeSpecialSummoned(e,ty,tp,check,lim,pos,sp)
  end
end

function Duel.DiscardRandomFromHand(p,c,r)
  c=c or 1
  r=r or REASON_EFFECT
  local g=Duel.GetFieldGroup(p,LOCATION_HAND,0)
  if g:IsEmpty() then return 0,Group.CreateGroup() end
  g=g:RandomSelect(p,c)
  return g:Discard(r),g
end

function Duel.PhaseCheck(phase,phase2)
  if type(phase)=="table" then
    return table.any(C(math.equal,Duel.GetCurrentPhase))
  elseif not phase2 then
    return Duel.GetCurrentPhase()==phase
  else
    return Duel.GetCurrentPhase()>=phase and Duel.GetCurrentPhase()<=phase2
  end
end

function Duel.BattlePhaseCheck()
  return Duel.GetCurrentPhase()>=PHASE_BATTLE_START and Duel.GetCurrentPhase()<=PHASE_BATTLE
end

function Duel.TurnCheck(p)
  return Duel.GetTurnPlayer()==p
end

function Duel.GetPendulums(p)
  local f=AF(Card.IsFaceup,OCF(Card.IsSequence,6,Card.IsSequence,7))
  return Duel.GetMatchingGroup(f,p,LOCATION_SZONE)
end

function Duel.HintSM(tp,msg)
  Duel.Hint(HINT_SELECTMSG,tp,msg)
end

function Duel.GetOperatedCard()
  return Duel.GetOperatedGroup():GetFirst()
end

function Duel.CheckFlagEffect(tp,flag,n,f)
  n=n or 0
  f=f or math.not_equal
  return f(Duel.GetFlagEffect(tp,flag),n)
end

function Duel.CheckFlagUnset(tp,flag,n,f)
  n=n or 0
  f=f or math.equal
  return f(Duel.GetFlagEffect(tp,flag),n)
end

local gfg=Duel.GetFieldGroup
function Duel.GetFieldGroup(p,s,o)
  if p==nil then o=o or s
    else o=o or 0 end
  return gfg(p,s,o)
end

local gfgc=Duel.GetFieldGroupCount
function Duel.GetFieldGroupCount(p,s,o)
  if p==nil then o=o or s
    else o=o or 0 end
  p=p or 0
  return gfgc(p,s,o)
end

function Duel.HasFieldGroupCount(p,s,o,n)
  n=n or 1
  return Duel.GetFieldGroupCount(p,s,o)>=n
end

function Duel.AnnounceFromRange(p,s,e)
  local vals={}
  for n=0,(e-s) do
    table.insert(vals,n+s)
  end
  return Duel.AnnounceNumber(p,table.unpack(vals))
end

function Duel.AnnounceFilteredNumber(f,p,s,e,...)
  local vals={}
  for n=0,(e-s) do
    table.insert(vals,n+s)
  end
  table.remove_unless(vals,f,...)
  return Duel.AnnounceNumber(p,table.unpack(vals))
end

local drcvr=Duel.Recover
function Duel.Recover(p,v,r,s)
  r=r or REASON_EFFECT
  if type(r)=="boolean" then r=REASON_EFFECT s=r end
  return drcvr(p,v,r,s)
end

local ddmg=Duel.Damage
function Duel.Damage(p,v,r,s)
  r=r or REASON_EFFECT
  if type(r)=="boolean" then r=REASON_EFFECT s=r end
  return ddmg(p,v,r,s)
end

local nsum=Duel.NegateSummon
function Duel.NegateSummon(t)
  local id=Config.GetValueOrDefault("negate-summon-event-id",false)
  nsum(t)
  if not id then return end
  if type(t)=="card" then t=t:AsGroup() end
  t:RemoveUnless(Card.GetReasonEffect)
  if t:IsEmpty() then return end
  local fake_eff=Effect.GlobalEffect()
  --[[local tf=t:GetFirst()
  Duel.RaiseEvent(t,EVENT_CUSTOM+id,tf:GetReasonEffect(),tf:GetReason(),tf:GetReasonPlayer(),0,0)]]
  --[[t:LoopFor(function (c)
    Duel.RaiseSingleEvent(c,EVENT_CUSTOM+id,c:GetReasonEffect(),c:GetReason(),c:GetReasonPlayer(),0,0)
  end)]]
  Duel.RaiseEvent(t,EVENT_CUSTOM+id,fake_eff,0,0,0,0)
  t:LoopFor(function (c)
    Duel.RaiseSingleEvent(c,EVENT_CUSTOM+id,fake_eff,0,0,0,0)
  end)
end

local sntd=Duel.SendtoDeck
function Duel.SendtoDeck(t,p,s,r,e,...)
  rp=e and e:GetHandlerPlayer() or 0
  e=e or Effect.GlobalEffect()
  local evt=Config.GetValueOrDefault("to-deck-event-id",false)
  local r=sntd(t,p,s,r,...)
  if evt then
    Duel.RaiseEvent(t,EVENT_CUSTOM+evt,e,r,rp,p,s)
    if type(t)=="group" then
      t:LoopFor(function (c)
        Duel.RaiseSingleEvent(c,EVENT_CUSTOM+evt,e,r,rp,p,s)
      end)
    else
      Duel.RaiseSingleEvent(t,EVENT_CUSTOM+evt,e,r,rp,p,s)
    end
  end
  return r
end

function Duel.DamageX(tp,s,o,r,st)
  o=o or s
  r=r or REASON_EFFECT
  Duel.Damage(tp,s,r,st)
  Duel.Damage(1-tp,o,r,st)
end

function Duel.SelectCardOption(p,id,...)
  local args=table.pack(...)
  args=table.map(args,Flip(aux.Stringid),id)
  return Duel.SelectOption(p,table.unpack(args))
end

function Duel.RegisterClone(e,tp,...)
  local cl=e:Clone(...)
  Duel.RegisterEffect(cl,tp)
  return cl
end

function Duel.GetCardsOnField()
  return Duel.GetFieldGroup(nil,LOCATION_ONFIELD)
end

function Duel.GetDecktop(tp)
  return Duel.GetDecktopGroup(tp,1):GetFirst()
end

function Duel.HasActivityCount(p,a,n,f)
  n=n or 0
  if f==nil then f=math.greater end
  return f(Duel.GetActivityCount(p,a),n)
end

function Duel.NotActivityCount(...)
  return not Duel.HasActivityCount(...)
end

function Duel.MaybeGetFieldCard(...)
  local tc=Duel.GetFieldCard(...)
  return tc and Maybe.Just(tc) or Maybe.Nothing()
end

function Duel.HintC(id)
  Duel.Hint(HINT_CARD,0,id)
end

function Duel.CheckLP(tp,n)
  return Duel.GetLP(tp)>=n
end

function Duel.SubLP(tp,n)
  Duel.SetLP(tp,Duel.GetLP(tp)-n)
  return Duel.GetLP(tp)
end

function Duel.AddLP(tp,n)
  Duel.SetLP(tp,Duel.GetLP(tp)+n)
  return Duel.GetLP(tp)
end

function Duel.GetAllCards()
  return Duel.GetFieldGroup(0,LOCATION_ALL,LOCATION_ALL)
end
