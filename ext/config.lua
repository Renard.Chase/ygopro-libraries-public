-- Vertex's Scripting Library
local io=require "io"
local src="config"
Extensions.Register(src,1,0,"Configuation Loader")
Extensions.Require(src,"maybe",1,0)
Extensions.Require(src,"io",1,0)
Extensions.Require(src,"string",1,0)

local config={}
Config={}

setmetatable(Config,{type="Configuration Functions"})

local function read_conf(file) 
  if not file_exists(file) then return end
  for line in io.lines(file) do
    line=line:trim()
    r=line:gsub(" ?= ?","="):split("=")
    if r[1]:sub(1,1)~="#" then
      config[r[1]:trim()]=r[2]:trim()
    end
  end
end

read_conf("system.conf")
read_conf("xsystem.conf")

function Config.LoadFile(file)
  read_conf("config/"..file..".conf")
end

function Config.HasValue(v)
  return config[v]~=nil
end

function Config.GetValue(v)
  if not Config.HasValue(v) then error("Configuration Value \""..v.."\" does not exist",2) end
  return config[v]
end

function Config.GetBoolean(b)
  local v=Config.GetValue(b)
  return v:lower()=="true"
end

function Config.GetInteger(i)
  local v=Config.GetValue(i)
  return tonumber(v)
end

function Config.MaybeGetValue(v)
  if Config.HasValue(v) then
    return Maybe.Just(Config.GetValue(v))
  else
    return Maybe.Nothing()
  end
end

function Config.MaybeGetBoolean(b)
  local v=Config.MaybeGetValue(b)
  return v:WithValue(Compose(string.lower,math.cequal("true")))
end

local function MtoI(m)
  local i=tonumber(m)
  local r=i==nil and Maybe.Nothing() or Maybe.Just(i)
  return r
end

function Config.MaybeGetInteger(i)
  local v=Config.MaybeGetValue(i)
  return v:WithValueOrDefault(MtoI,Maybe.Nothing())
end

function Config.GetValueOrDefault(v,d)
  return Config.HasValue(v) and Config.GetValue(v) or d
end

function Config.GetBooleanOrDefault(v,d)
  local m=Config.MaybeGetBoolean(v)
  return m:GetValueOrDefault(d)
end

function Config.GetIntegerOrDefault(v,d)
  local m=Config.MaybeGetInteger(v)
  return m:GetValueOrDefault(d)
end

function Config.GetConfigInfo()
  if CheckDebugFlag() then error("Configuation Loader Error : Config.GetConfigInfo, debug flag must be enabled.",2) end
  return config
end
