-- Vertex's Scripting Library
local src="errata"
Errata={}
Errata.check={}
Errata.change={}
Errata.filter={}

Extensions.Register(src,1,0,"Card Errata Library")

local rg=Card.RegisterEffect
function Card.RegisterEffect(c,e,...)
  if c then
    if Errata.check[c:GetOriginalCode()] and Errata.check[c:GetOriginalCode()](c,e,...) then
      local er=Errata.change[c:GetOriginalCode()]
      local ne=er(c,e,...)
      return rg(c,ne,...)
    else
      for _,v in ipairs(Errata.filter) do
        local cf,ef=v[1],v[2]
        if cf(c,e,...) then
          local ne=ef(c,e,...)
          return rg(c,ne,...)
        end
      end
    end
  end
  return rg(c,e,...)
end
