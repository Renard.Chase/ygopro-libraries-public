-- Vertex's Scripting Library
local src="function-short"
Extensions.Register(src,1,4,"Function Extension Shortcuts")
Extensions.Require(src,"function",1,5)

DF=DoFunctions
DCF=DoCurryFunctions
AF=AndFunctions
ACF=AndCurryFunctions
OF=OrFunctions
OCF=OrCurryFunctions
FA=ForArguments
CCF=CreateCheckedFunction
CCHF=CreateCheckedHandlerFunction
MOF=MergeOperationFunctions
C=Curry
CS=CurryS
CR=CurryR
CCurry=CardCurry
CC=CardCurry
CCS=CardCurryS
CCR=CardCurryR
CIC=ChainInfoCurry
CMP=Compose
VF=ValueOnlyFunction
OA=OrderArguments
R=Reverse
N=Negate
WH=WithHandler
WC=WithConfirm
NOP=NoOperation
SF=ShiftFunction
SA=ShiftArguments
SSA=SingleShiftArgument
ASF=AndShiftFunctions
OSF=OrShiftFunctions
DSF=DoShiftFunctions
PF=PrepareFunction
PCF=PrepareCardFunction
