-- Vertex's Scripting Library
local data={}
local debug=false
Loading={}
Loading.Pre={}
Loading.Post={}
Extensions={}
Extensions.Loaded=true
Extensions.LoadedOnce=false
Extensions.Error={
  NO_ERROR=1,
  NOT_LOADED=-1,
  INVALID_MAJOR_VERSION=-2,
  INVALID_MINOR_VERSION=-3,
  UNSUPPORTED_VERSION=-4}
  
function EnableDebugFlag()
  debug=true
  Debug.Message("Debug Flag Enabled")
end

function CheckDebugFlag()
  return not debug
end

function IsDebugMode()
  return debug
end

function Extensions.Register(_id,_major,_minor,_name)
  _name=_name or _id
  if data[_id] then Debug.Message(table.concat({"Extensions.Register Error : ","Extension \"",_id,"\" already exists."})) return false end
  if not (_id or _major or _minor) then Debug.Message("Extensions.Register Error : 3 Arguments required.") return false end
  if not type(_major)=="number" then Debug.Message("Extensions.Register Error : Major Version Number is not a number") return false end
  if not type(_minor)=="number" then Debug.Message("Extensions.Register Error : Minor Version Number is not a number") return false end
  local dat={
              name=_name,
              major=_major,
              minor=_minor,
              req={}} 
  data[_id]=dat
  return true
end

function Extensions.Require(_src,_id,_major,_minor)
  if not data[_src] then Debug.Message(table.concat({"Extensions.Require Error : Extension \"",_src,"\" does not exist."})) end
  table.insert(data[_src].req,{id=_id,major=_major,minor=_minor})
end

function Extensions.GetData()
  if not CheckDebugFlag() then Debug.Message("Extension Error : Extension.GetData, debug flag must be enabled.") end
  return data
end

function Extensions.SetMinVersion(_id,_version)
  if not data[_id] then Debug.Message(table.concat({"Extensions.SetMinVersion Error : Extension \"",_id,"\" does not exist."})) end
  data[_id].min=_version
end

local function compReq(r,d)
  if r.major>d.major then return false,Extensions.Error.INVALID_MAJOR_VERSION end
  if r.major==d.major and r.minor>d.minor then return false,Extensions.Error.INVALID_MINOR_VERSION end
  if d.min then
    if r.major<d.min then return false,Extensions.Error.UNSUPPORTED_VERSION end
  end
  return true,Extensions.Error.NO_ERROR
end

local function checkReq(r)
  local req=r.req
  for _,v in ipairs(req) do
    local dat=data[v.id]
    if not dat then return false,v.id,Extensions.Error.NOT_LOADED,v end
    local chk,err=compReq(v,dat)
    if not chk then return false,v.id,err,v end
  end
  return true,nil,Extensions.Error.NO_ERROR,nil
end

function Extensions.Check()
  for k,v in pairs(data) do
    local chk,id,err,req=checkReq(v)
    if not chk then
      local function cm(...) Debug.Message(table.concat(table.pack(...))) end
      if err==Extensions.Error.NOT_LOADED then
        cm("Extension Error : Extension \"",v.name,"\" requires Extension \"",id,"\" which is not loaded.")
      elseif err==Extensions.Error.INVALID_MAJOR_VERSION or err==Extensions.Error.INVALID_MINOR_VERSION then
        cm("Extension Error : Extension \"",v.name,"\" requires Extension \"",id,"\" of unsupported version \"",req.major,".",req.minor,"\". Version \"",data[id].major,".",data[id].minor,"\" is loaded.")
      elseif err==Extensions.Error.UNSUPPORTED_VERSION then
        cm("Extension Error : Extension \"",v.name,"\" requires Extension \"",id,"\" of unsupported version \"",req.major,".",req.minor,"\". Only versions of \"",data[id].min,".0\" or higher are supported.")
      elseif err==Extensions.Error.NO_ERROR then
        cm("Extension Error : Extension \"",v.name,"\" requires extensions but checkReq reported an invalid extension but reported that there was not an error. This is almost certainly a bug, please report it to the developer of your version of the Extensions Extension.")
      else
        cm("Extension Error : Extension \"",v.name,"\" requires extension but checkReq returned an invalid error code, please contact your developer of the Extensions Extension.")
      end
      Extensions.Loaded=false
      return false
    end
  end
  Extensions.LoadedOnce=true
  return true
end

function Card.initial_effect(c)
  local ref=_G['c' .. c:GetOriginalCode()]
  local s=ref.start
  ref.initial_effect=function (crd)
    if not Extensions.Loaded then return end
    for _,v in ipairs(Loading.Pre) do
      v(crd)
    end
    if s then s(crd) end
    for _,v in ipairs(Loading.Post) do
      v(crd)
    end
  end
  ref.initial_effect(c)
end

Extensions.Register("ext",1,0,"Extensions")
