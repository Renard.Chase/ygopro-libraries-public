-- Vertex's Scripting Library
--[[ Ritual Summon Library.
Functions:
  RitualSummon.CreateSummon()
  RitualSummon.SetMaterials(rs,mat,def)
  RitualSummon.AddMaterialFilter(rs,filter (c,rc,e,tp))
  RitualSummon.SetSummonFilter(rs,filter)
  RitualSummon.SetMaterialFilter(rs,filter)
  RitualSummon.SetLevelOffset(rs,offset)
  RitualSummon.EnableZoneCheck(rs,enable)
  RitualSummon.BreakEffectOnSummon(rs,ebreak)
  RitualSummon.SetSummonPosition(rs,pos)
  RitualSummon.RequireExactLevels(rs,req)
  RitualSummon.SetRitualMonsterLocation(rs,loc)
  RitualSummon.AllowNonRitualMonsters(rs,allow)
  RitualSummon.DisableRitualMaterialFilterOnRitualMonster(rs,disable)
  RitualSummon.SetDestination(rs,source,destination,option,release)
  RitualSummon.SetRequiredMaterials(rs,mats)
  
  RitualSummon.Check(rs,e,tp)
  RitualSummon.GetFilter(rs)
  RitualSummon.Filter(rs,c,e,tp)
  RitualSummon.Summon(rs,tp)

  RitualSummon.FilterRitualGroup(rs,g,rc)
  RitualSummon.CardFilter(rs,c,rc)
  RitualSummon.CheckRitualLevels(rs,c,mat_group,target_level,rc)
]]
local src="ritual"
Extensions.Register(src,2,0,"Ritual Summon Library")
Extensions.Require(src,"function",1,5)
Extensions.Require(src,"function-short",1,4)
Extensions.Require(src,"card",1,6)
Extensions.Require(src,"group",1,3)
Extensions.Require(src,"duel",1,3)
Extensions.Require(src,"math",1,4)
Extensions.Require(src,"maybe",1,0)
Extensions.Require(src,"constant",1,1)
Extensions.Require(src,"type",1,0)
Extensions.SetMinVersion(src,2)

local rsn=REASON_EFFECT+REASON_RITUAL+REASON_MATERIAL

RITUAL_LEVEL_EQUAL=0
RITUAL_LEVEL_GREATER=1

RitualSummon={}

local function RitualSummonValue()
  return "Ritual Summon"
end

-- Creates a new Ritual Summon Object
function RitualSummon.CreateSummon()
  local rs={}
  setmetatable(rs,{__index=RitualSummon,
                    __tostring=RitualSummonValue,
                    __type="ritual-summon"
              });
  rs.mat=Group.CreateGroup()
  rs.use_default_materials=true
  rs.summon_filter_function=aux.TRUE
  rs.material_filter_function=aux.TRUE
  rs.offset=0
  rs.zone_check=true
  rs.ebreak=false
  rs.summon_position=POS_FACEUP
  rs.exact=false
  rs.monster_location=LOCATION_HAND
  rs.any_monster=false
  rs.ignore_filter=false
  rs.required=Group.CreateGroup()
  rs.get_material_filters={}
  rs.destinations={}
  rs.destinations[LOCATION_HAND]={Card.IsCanBeRitualMaterial,Card.ReleaseRitualMaterial,HINTMSG_RELEASE}
  rs.destinations[LOCATION_MZONE]=rs.destinations[LOCATION_HAND]
  rs.destinations[LOCATION_SZONE]=rs.destinations[LOCATION_HAND]
  rs.destinations[LOCATION_GRAVE]={function (tc) return tc:IsAbleToRemove() end,function (tc) tc:Banish(POS_FACEUP,REASON_RITUAL+REASON_MATERIAL) end,HINTMSG_REMOVE}
  rs.destinations[LOCATION_EXTRA]={Card.IsAbleToGrave,CC(Group.SendtoGrave,rsn),HINTMSG_TOGRAVE}
  rs.destinations[LOCATION_DECK]=rs.destinations[LOCATION_EXTRA]
  rs.destinations[LOCATION_REMOVED]=rs.destinations[LOCATION_EXTRA]

  rs.mat:KeepAlive()
  rs.required:KeepAlive()

  return rs
end

-- Copies the given ritual summon into a new object
function RitualSummon.Clone(rs)
  local n=RitualSummon.CreateSummon()
  n.mat:DeleteGroup()
  n.mat=rs.mat:Clone()
  n.use_default_materials=rs.use_default_materials
  n.summon_filter_function=rs.summon_filter_function
  n.material_filter_function=rs.material_filter_function
  n.offset=rs.offset
  n.zone_check=rs.zone_check
  n.ebreak=rs.ebreak
  n.summon_position=rs.summon_position
  n.exact=rs.exact
  n.monster_location=rs.monster_location
  n.any_monster=rs.any_monster
  n.ignore_filter=rs.ignore_filter
  n.required:DeleteGroup()
  n.required=rs.required:Clone()
  for _,v in pairs(rs.get_material_filters) do
    table.insert(n.get_material_filters,v)
  end
  for k,v in pairs(rs.destinations) do
    n.destinations[k]=v
  end

  n.mat:KeepAlive()
  n.required:KeepAlive()

  return n
end

-- Sets additional materials to be used for the summon
function RitualSummon.SetMaterials(rs,mat,def)
  if type(mat)=="card" then mat=mat:AsGroup() end
  if type(mat)~="group" then Debug.Message("RitualSummon SetMaterials Error : material group (argument 2) was not a group.") return rs end
  rs.materials:DeleteGroup()
  rs.materials=mat
  rs.materials:KeepAlive()
  rs.use_default_materials=def==nil and true or def
  return rs
end

-- Sets function to get additional materials
function RitualSummon.AddMaterialFilter(rs,f)
  table.insert(rs.get_material_filters,f)
  return rs
end

-- Sets filter to check monster to summon with
function RitualSummon.SetSummonFilter(rs,filter)
  if type(filter)~="function" then Debug.Message("RitualSummon SetSummonFilter Error : ritual monster filter function (argument 2) was not a function.") return rs end
  rs.summon_filter_function=filter
  return rs
end

-- Sets filter to check materials with
function RitualSummon.SetMaterialFilter(rs,filter)
  if type(filter)~="function" then Debug.Message("RitualSummon SetMaterialFilter Error : ritual material filter function (argument 2) was not a function.") return rs end
  rs.material_filter_function=filter
  return rs
end

-- Adjusts the total level of materials that you need to summon with. If this is, say, set to 2, then a Level 5 monster would take 7 Levels of materials.
function RitualSummon.SetLevelOffset(rs,offset)
  if type(offset)~="number" then Debug.Message("RitualSummon SetLevelOffset Error : ritual monster level offset value (argument 2) was not a number.") return rs end
  rs.offset=offset
  return rs
end

-- Checks for available room during the Ritual Summon, if false then Ritual Summon will be attempted even if there are no free monster zones.
function RitualSummon.EnableZoneCheck(rs,enable)
  if type(enable)~="boolean" then Debug.Message("RitualSummon EnableZoneCheck Error : use zone check value (argument 2) was not a boolean.") return rs end
  rs.zone_check=enable
  return rs
end

-- Set to "true" if tributing materials and summoning the monster don't happen at the same time
function RitualSummon.BreakEffectOnSummon(rs,ebreak)
  if type(ebreak)~="boolean" then Debug.Message("RitualSummon BreakEffectOnSummon Error : break effect value (argument 2) was not a boolean.") return rs end
  rs.ebreak=ebreak
  return rs
end

-- Sets the Battle Position of the summoned monster
function RitualSummon.SetSummonPosition(rs,pos)
  if type(pos)~="number" then Debug.Message("RitualSummon SetSummonPosition Error : summon position value (argument 2) was not a number") return rs end
  if pos<=0 or pos>=0x10 then Debug.Message("RitualSummon SetSummonPosition Error : summon position value (argument 2) was not a valid value") return rs end
  rs.summon_position=pos
  return rs
end

-- If "true" then you will need to Tribute monsters whose Levels exactly equal the Level of the summoned monster. If "false" you need only Tribute monsters whose Levels equal or exceed the target.
function RitualSummon.RequireExactLevels(rs,req)
  if type(req)~="boolean" then Debug.Message("RitualSummon RequireExactLevels Error : require levels to be exact value (argument 2) was not a boolean") return rs end
  rs.exact=req
  return rs
end

-- Sets the Location to look for the monster to Ritual Summon
function RitualSummon.SetRitualMonsterLocation(rs,loc)
  if type(loc)~="number" then Debug.Message("RitualSummon SetRitualMonsterLocation Error : location value (argument 2) was not a number") return rs end
  if loc<=0 or loc>=0x100 then Debug.Message("RitualSummon SetRitualMonsterLocation Error : location value (argument 2) was nto a valid location") return rs end
  rs.monster_location=loc
  return rs
end

-- Allows the Ritual Summoning of non-Ritual Monsters
function RitualSummon.AllowNonRitualMonsters(rs,allow)
  if type(allow)~="boolean" then Debug.Message("RitualSummon AllowNonRitualMonsters Error : allow value (argument 2) was not a boolean") return rs end
  rs.any_monster=allow
  return rs
end

-- Allows the Ritual Summon to ignore the Ritual Monster's inherent filters for its materials.
function RitualSummon.DisableRitualMaterialFilterOnRitualMonster(rs,disable)
  if type(disable)~="boolean" then Debug.Message("RitualSummon DisableRitualMaterialFilterOnRitualMonster Error : disable value (argument 2) was not a boolean") return rs end
  rs.ignore_filter=disable
  return rs
end

-- Sets the destination and check functions for cards from the given source. If destination is nil, then the function treats it as a standard tribute for a ritual summon.
function RitualSummon.SetDestination(rs,source,destination,opt,release)
  local reason=reason+(release and REASON_RELEASE or 0)
  local fs={}
  if destination==nil then fs={Card.IsCanBeRitualMaterial,Group.ReleaseRitualMaterial,HINTMSG_RELEASE}
    elseif destination==LOCATION_HAND then fs={Card.IsAbleToHand,CCR(Card.SendtoHand,nil,reason),opt and HINTMSG_ATOHAND or HINTMSG_RTOHAND}
    elseif destination==LOCATION_GRAVE then fs={Card.IsAbleToGrave,CC(Card.SendtoGrave,reason),HINTMSG_TOGRAVE}
    elseif destination==LOCATION_REMOVED then fs={function (tc) return tc:IsAbleToRemove() end,
                                                    function (tc) tc:Banish(opt or POS_FACEUP,reason) end,HINTMSG_REMOVE}
    elseif destinations==LOCATION_DECK then fs={Card.IsAbleToDeck,
                                                  function (tc) tc:SendtoDeck(nil,opt or DECK_ORDER_SHUFFLE,reason) end,HINTMSG_TODECK}
    end
    return rs
end

-- Sets materials that must be used for the ritual summon
function RitualSummon.SetRequiredMaterials(rs,mats)
  if type(mats)=="card" then mats=mats:AsGroup() end
  if not type(mats)=="group" then Debug.Message("RitualSummon SetRequiredMaterials Error : required materials (argument 2) was not a group") return rs end
  rs.required:DeleteGroup()
  rs.required=mats
  rs.required:KeepAlive()
  return rs
end

-- Checks for summonable ritual monster
function RitualSummon.Check(rs,e,tp,is_pre_op)
  if is_pre_op==nil then is_pre_op=true end
  return Duel.PlayerFilterCheck(rs:GetFilter(),tp,rs.monster_location,nil,e,tp,is_pre_op)
end

-- Gets a version of RitualSummon.Filter that can be used in other function calls
function RitualSummon.GetFilter(rs)
  return C(RitualSummon.Filter,rs)
end

-- Filter to see if card (c) can be Ritual Summon with this Summoning Method.
function RitualSummon.Filter(rs,c,e,tp,is_pre_op)
  if is_pre_op==nil then is_pre_op=true end
  -- Checks
  local isMonster=c:IsType(TYPE_MONSTER)
  local isRitual=c:IsType(TYPE_RITUAL) or rs.any_monster
  local canSummon=c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_RITUAL,tp,false,true,rs.summon_position)
  local hasFilter=rs.summon_filter_function(c,e,tp,is_pre_op)
  local hasRequired=false
  local hasZone=true
  local hasMaterial=false

  -- Get Materials
  local g=rs.use_default_materials and Duel.GetRitualMaterial(tp) or Group.CreateGroup()
  local additional_material_group=Duel.GetFieldGroup(nil,0xFF)
  additional_material_group:RemoveUnless(OF(table.unpack(rs.get_material_filters)),c,e,tp,is_pre_op)
  g:Merge(additional_material_group:DifferenceThis(g))
  g=rs:FilterRitualGroup(g,c,e,tp,is_pre_op)
  
  -- Get Offset
  local offset=rs.offset
  
  -- Calculate Required Materials
  hasRequired=rs.required:IsEmpty() or (rs.required:All(rs.material_filter_function,c,e,tp,is_pre_op) and rs.required:All(C(RitualSummon.FilterAgainstRitual,rs),c))
  offset=rs.required:SubBy(Card.GetRitualLevel,offset,c)
  g:RemoveCard(rs.required)
  
  -- Calculate Target Level
  local target_level=c:GetLevel()+offset

  -- Check Monster Zone
  if not Duel.ZoneCheck(tp) and target_level>0 then
    local tg=g:Filter(Card.IsLocation,nil,LOCATION_MZONE)
    hasZone=rs.required:Any(Card.IsLocation,LOCATION_MZONE)
    hasZone=hasZone or tg:Any(C(rs.CardFilter,rs),c)
  elseif not Duel.ZoneCheck(tp) and target_level<=0 then
    hasZone=rs.required:Any(Card.IsLocation,LOCATION_MZONE)
  end
  hasZone=hasZone or not rs.zone_check
  
  -- Material Check
  if target_level<=0 then
    hasMaterial=target_level==0 or not rs.exact
  else
    if rs.exact then hasMaterial=g:CheckWithSumEqual(Card.GetRitualLevel,target_level,1,99,c)
      else hasMaterial=g:CheckWithSumGreater(Card.GetRitualLevel,target_level,c) end
  end

  -- Return
  return isMonster and isRitual and canSummon and hasFilter and hasRequired and hasZone and hasMaterial
end

function RitualSummon.Summon(rs,e,tp)
  -- Offset
  local offset=rs.offset
  
  -- Get Ritual Monster
  local sg=Duel.GetMatchingGroup(C(RitualSummon.Filter,rs),tp,rs.monster_location,0,nil,e,tp,false)
  if sg:IsEmpty() then return end
  Duel.HintSM(tp,HINTMSG_SPSUMMON)
  local sc=sg:SelectSingleCard(tp)

  -- Calculate Offset
  offset=rs.required:SubBy(Card.GetRitualLevel,offset,sc)
  local level=sc:GetLevel()+offset

  -- Get Materials
  local mg=rs.use_default_materials and Duel.GetRitualMaterial(tp) or Group.CreateGroup()
  mg:RemoveCard(rs.required)
  local additional_material_group=Duel.GetFieldGroup(nil,0xFF)
  additional_material_group:RemoveUnless(OF(table.unpack(rs.get_material_filters)),c,e,tp,false)
  mg:Merge(additional_material_group:DifferenceThis(mg))
  mg=rs:FilterRitualGroup(mg,sc,e,tp,false)

  -- Free MZone if Full
  local mz=Maybe.Nothing()
  if rs.required:None(Card.IsLocation,LOCATION_MZONE) and not Duel.ZoneCheck(tp) then
    local zg=mg:DropUnless(Card.IsLocation,LOCATION_MZONE)
                :ExcludeUnless(C(RitualSummon.CheckRitualLevels,rs),mg,level,sc)
    if zg:IsEmpty() then if IsDebugMode() then Debug.Message("DEBUG Ritual Summon : MZone Full, all materials excluded.") end return end
    Duel.HintSM(tp,rs.destinations[LOCATION_MZONE][3])
    local zc=zg:SelectSingleCard(tp)
    mg:RemoveCard(zc)
    mz=Maybe.Just(zc)
  end
  level=level-(mz:WithValueOrDefault(CC(Card.GetRitualLevel,sc),0))
  
  -- Select Materials
  if (level>0) then
    Duel.HintSM(tp,HINTMSG_RELEASE)
    if rs.exact then mg=mg:SelectWithSumEqual(tp,Card.GetRitualLevel,level,1,127,sc)
      else mg=mg:SelectWithSumGreater(tp,Card.GetRitualLevel,level,sc) end
  else
    mg=rs.required
  end

  -- Merge Materials
  mz:OnValue(Flip(Group.Merge),mg)
  mg:Merge(rs.required)

  -- Use Materials
  sc:SetMaterial(mg)
  mg:LoopFor(function (tc)
    local loc=tc:GetLocation()
    rs.destinations[loc][2](tc)
  end)

  -- Break
  if rs.ebreak then Duel.BreakEffect() end

  -- Ritual Summon
  sc:SpecialSummon(SUMMON_TYPE_RITUAL,tp,tp,false,true,rs.summon_position)
  sc:CompleteProcedure()
  
  -- Return
  return sc,mg
end

-- Removes non-Ritual Materials from Ritual Group
function RitualSummon.FilterRitualGroup(rs,g,rc,e,tp,is_pre_op)
  g:RemoveUnless(rs.material_filter_function,rc,e,tp,is_pre_op)
  g:Merge(rs.mat)
  g:RemoveUnless(C(RitualSummon.CardFilter,rs),rc,e,tp)
  g:RemoveCard(rc)
  return g
end

-- Checks if card is valid material for this Summon
function RitualSummon.CardFilter(rs,c,rc)
  local canBeMaterial=rs.destinations[c:GetLocation()][1](c,rc)
  local hasFilter=rs:FilterAgainstRitual(c,rc)
  return canBeMaterial and hasFilter
end

function RitualSummon.FilterAgainstRitual(rs,c,rc)
  return rs.ignore_filter or not rc.ritual_filter or rc.ritual_filter(c)
end

-- Checks if card has the right level to be used for the Ritual Summon.
function RitualSummon.CheckRitualLevels(rs,c,mat_group,target_level,rc)
  local sel_group=mat_group:DropCard(c)
  local tl=target_level-c:GetRitualLevel(rc)
  if tl==0 then return true end
  if tl<=0 then return not rs.exact end
  local hasSum=false
  if rs.exact then hasSum=sel_group:CheckWithSumEqual(Card.GetRitualLevel,tl,1,99,rc)
    else hasSum=sel_group:CheckWithSumGreater(Card.GetRitualLevel,tl,rc) end
  
  return hasSum
end
