-- Vertex's Scripting Library
Extensions.Register("effect-maybe",1,0,"Effect Get Maybe Functions")
Extensions.Require("effect-maybe","maybe",1,0)

local gf=function (f)
  return function (...)
    local r=f(...)
    return r~=nil and Maybe.Just(r) or Maybe.Nothing()
  end
end

Effect.MaybeGetCondition=gf(Effect.GetCondition)
Effect.MaybeGetTarget=gf(Effect.GetTarget)
Effect.MaybeGetCost=gf(Effect.GetCost)
Effect.MaybeGetValue=gf(Effect.GetValue)
Effect.MaybeGetOperation=gf(Effect.GetOperation)
