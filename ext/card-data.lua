-- Vertex's Scripting Library
local src="card-data"
Extensions.Register(src,1,0,"Card Data Functions")
Extensions.Require(src,"maybe",1,0)

local guid_flag_id=Config.GetIntegerOrDefault("card-data-guid-flag-id",99999999)
local next_id=0
local data={}
local flags={}

local function initalize_data(c)
  next_id=next_id+1
  data[next_id]={}
  flags[next_id]={}
  c:RegisterFlagEffect(guid_flag_id,0,EFFECT_FLAG_IGNORE_IMMUNE+NO_COPY_NO_DISABLE+EFFECT_FLAG_CANNOT_NEGATE,0,next_id)
  c:SetFlagEffectLabel(guid_flag_id,next_id)
end

table.insert(Loading.Pre,initalize_data)

local function get_guid(c)
  local guid=c:GetFlagEffectLabel(guid_flag_id)
  if guid==nil then initalize_data(c) return get_guid(c) end
  return c:GetFlagEffectLabel(guid_flag_id)
end

function Card.HasData(c,d)
  local guid=get_guid(c)
  return data[guid][d]~=nil
end

function Card.GetData(c,d)
  local guid=get_guid(c)
  if not c:HasData(d) then error("Card Data Error : Data Value \""..(type(d)=="string" or "") .."\" does not exist",2) return nil end
  return data[guid][d]
end

function Card.MaybeGetData(c,d)
  if c:HasData(d) then
    return Maybe.Just(c:GetData(d))
  end
  return Maybe.Nothing()
end

function Card.GetDataOrDefault(c,d,def)
  return c:HasData(d) and c:GetData(d) or def
end

function Card.SetData(c,d,v)
  local guid=get_guid(c)
  data[guid][d]=v
end

function Card.SetDataFlag(c,f)
  local guid=get_guid(c)
  local hadFlag=flags[guid][f]==true
  flags[guid][f]=true
  return hadFlag
end

function Card.HasDataFlag(c,f)
  local guid=get_guid(c)
  return flags[guid][f]
end

function Card.RemoveDataFlag(c,f)
  local guid=get_guid(c)
  local hadFlag=flags[guid][f]==true
  flags[guid][f]=nil
  return hadFlag
end
