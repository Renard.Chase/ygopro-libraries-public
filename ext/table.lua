-- Vertex's Scripting Library
local src="table"
Extensions.Register(src,1,4,"table")
Extensions.Require(src,"group",1,0)
Extensions.Require(src,"math",1,0)
Extensions.Require(src,"function",1,3)

function table.fold(t,f,b,...)
  local hasItme=false
  local r=b
  for k,v in ipairs(t) do
    r=f(r,v,...)
  end
  return r
end

function table.map(t,f,...)
  local r={}
  for _,v in ipairs(t) do
    table.insert(r,f(v,...))
  end
  return r
end

function table.mapG(...)
  return Group.CreateFromTable(table.map(...))
end

function table.contains(t,i)
  return table.any(t,math.equal(i))
end

function table.all(t,f,...)
  f=f or Echo
  local r=true
  for _,v in pairs(t) do
    r=f(v,...)
    if not r then break end
  end
  return r
end

function table.any(t,f,...)
  f=f or Echo
  local r=false
  for _,v in pairs(t) do
    r=f(v,...)
    if r then break end
  end
  return r
end

function table.clone(t)
  local c={}
  for k,v in pairs(t) do
    c[k]=v
  end
  return c
end

function table.merge_keys(t1,t2)
  for a,b in pairs(t2) do
    local has=false
    for x,y in pairs(t1) do
      if a==x then has=true break end
    end
    if has then
      t1[a]={t1[a],b}
    else
      t1[a]=b
    end
  end
end

function table.merge_array(t1,t2,allow_dup)
  if allow_dup==nil then allow_dup=true end
  for k,v in ipairs(t2) do
    if allow_dup or (not table.any(t1,math.equal,v)) then
      table.insert(t1,v)
    end
  end
end

function table.maxBy(t,f,...)
  return table.fold(t,function (acc,v,...) return f(v,...)>f(acc,...) and v or acc end,t[1],...)
end

function table.minBy(t,f,...)
  return table.fold(t,function (acc,v,...) return f(v,...)<f(acc,...) and v or acc end,t[1],...)
end

function table.sumBy(t,f,b,...)
  b=b or 0
  return table.fold(t,function (acc,v,...) return acc+f(v,...) end,b,...)
end

local function getLen(t)
  return #t
end

function table.zip(...)
  local args=table.pack(...)
  local r={}
  local len=table.map(args,getLen)
  local x=table.maxBy(len,function (x) return x end)
  for n=1,x do
    for m=1,args.n do
      table.insert(r[n],args[m][n])
    end
  end
  return r
end

function table.zipWith(f,...)
  local args=table.pack(...)
  local r={}
  local len=table.map(args,getLen)
  local x=table.maxBy(len,function (x) return x end)
  for n=1,x do
    local t={}
    for m=1,args.n do
      table.insert(t,args[m][n])
    end
    table.insert(r,f(table.unpack(t)))
  end
  return r
end

function table.print(t)
  for k,v in pairs(t) do
    Debug.Message((k or "") .. " : " .. (v or ""))
  end
end

function table.foreach(t,f,...)
  for _,v in pairs(t) do
    f(v,...)
  end
end

function table.clone_by(t,f,...)
  local t={}
  table.foreach(t,function (...)
    if f(v,...) then table.insert(t,v) end
  end,...)
  return t
end

function table.clone_unless(t,f,...)
  local t={}
  table.foreach(t,function (...)
    if not (f(v,...)) then table.insert(t,v) end
  end,...)
  return t
end

function table.remove_if(t,f,...)
  for k,v in pairs(t) do
    if f(v,...) then
      table.remove(t,k)
    end
  end
end

function table.remove_unless(t,f,...)
  table.remove_if(t,Negate(f),...)
end

function table.simple_iterator(t)
  local isFirst=true
  local n=1
  return function ()
    local v=t[n]
    n=n+1
    return v
  end
end
