-- Vertex's Scripting Library
Extensions.Register("math",1,4,"Math")
Extensions.Require("math","function",1,5)
local function mbf(x)
  return function (f,a,b)
    if a==nil and b==nil then
      return function (c,d) return x(f(c),f(d)) end
    end
    return x(f(a),f(b))
  end
end

function math.greater(a,b)
  if b==nil then return Curry(math.greater,a) end
  return a>b
end
math.greater_by=mbf(math.greater)

function math.less(a,b)
  if b==nil then return Curry(math.less,a) end
  return a<b
end
math.less_by=mbf(math.less)

function math.greater_eq(a,b)
  if b==nil then return Curry(math.greater_eq,a) end
  return a>=b
end
math.greater_eq_by=mbf(math.greater_eq)

function math.less_eq(a,b)
  if b==nil then return Curry(math.less_eq,a) end
  return a<=b
end
math.less_eq_by=mbf(math.less_eq)

function math.equal(a,b)
  return a==b
end
math.equals=math.equal
math.equal_by=mbf(math.equal)
math.equals_by=math.equal_by

function math.cequal(a)
  return Curry(math.equal,a)
end

function math.not_equal(a,b)
  return a~=b
end

function math.not_cequal(a)
  return Curry(math.equal,a)
end
math.not_equals=math.not_equal
math.not_equal_by=mbf(math.not_equal)
math.not_equals_by=math.not_equal_by

function math.add(a,b)
  if b==nil then return Curry(math.add,a) end
  return a+b
end
math.add_by=mbf(math.add)

function math.subtract(a,b)
  if b==nil then return Curry(math.subtract,a) end
  if a==nil then return Curry(Flip(math.subtract),b) end
  return a-b
end
math.subtract_by=mbf(math.subtract)

function math.multiply(a,b)
  if b==nil then return Curry(math.multiply,a) end
  return a*b
end
math.multiply_by=mbf(math.multiply)

function math.divide(a,b)
  if b==nil then return Curry(math.divide,a) end
  if a==nil then return Curry(Flip(math.divide),b) end
  return a/b
end
math.divide_by=mbf(math.divide)

function math.top(a,b)
  if b==nil then return Curry(math.top,a) end
  return a<b and b or a
end
math.top_by=mbf(math.top)

function math.bottom(a,b)
  if b==nil then return Curry(math.bottom,a) end
  return a>b and b or a
end
math.bottom_by=mbf(math.bottom)

function math.is_even(n)
  return n % 2 == 0
end
math.is_odd=Negate(math.is_even)

function math.negate(n)
  return n*-1
end
math.negative=math.negate
