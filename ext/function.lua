-- Vertex's Scripting Library
Extensions.Register("function",1,5,"Function")
function DoFunctions(...)
  local args=table.pack(...)
  return function(...)
    local n=1
    while n<=args.n do
      local v=args[n]
      v(...)
      n=n+1
    end
  end
end

function AndFunctions(...)
  local args=table.pack(...)
  return function(...)
    local res=true
    for n=1,args.n do
      if not args[n] then res=false break end
      local flg;flg,res=pcall(args[n],...)
      if not flg then error(res,2) end
      
      if not res then break end
    end
    if res then return true end
    return false
  end
end

function OrFunctions(...)
  local args={...}
  return function(...)
    local res=false
    for _,v in pairs(args) do
      res=v(...)
      if res then break end
    end
    if res then return true end
    return false
  end
end

function ForArguments(x,f,...)
  local args=table.pack(...)
  if (type(x)=="function") then
    return ForArguments(1,x,f,...)
  end
  local res={}
  for n=0,args.n do
    local rt=table.pack(f(args[n]))
    for m=0,rt.n do
      table.insert(res,rt[m])
    end
  end
  return res
end

local function CurryLoopFunctions(df)
  return function(...)
    local args=table.pack(...)
    local xf={}
    local n=1
    while n<=args.n do
      local f=args[n]
      n=n+1
      while n<=args.n and type(args[n])~="function" do
        f=CardCurry(f,args[n])
        n=n+1
      end
      table.insert(xf,f)
    end
    return df(table.unpack(xf))
  end
end

DoCurryFunctions=CurryLoopFunctions(DoFunctions)
AndCurryFunctions=CurryLoopFunctions(AndFunctions)
OrCurryFunctions=CurryLoopFunctions(OrFunctions)

function CreateCheckedFunction(func,checked,target)
  checked=checked or aux.TRUE
  local function f(e,s) if type(s)=="string" then local path=table.concat({"script/c",e:GetHandler():GetOriginalCode(),".lua"}) return Dynamic(s,path) else return s end end
  return function(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
    local x=Curry(f,e)
    if target then
      if chkc then
        return x(target)(e,tp,eg,ep,ev,re,r,rp,chkc)
      end
    end
    if chk==0 then
      return x(checked)(e,tp,eg,ep,ev,re,r,rp)
    end
    return x(func)(e,tp,eg,ep,ev,re,r,rp)
  end
end

function CreateCheckedHandlerFunction(func,checked,target)
  if func then func=WithHandler(func) end
  if checked then checked=WithHandler(checked) end
  if target then target=WithHandler(target) end
  return CreateCheckedFunction(func,checked,target)
end

function MergeOperationFunctions(...)
  local args=table.pack(...)
  return function (e,tp,eg,ep,ev,re,r,rp,chk,chkc)
    local res=true
    for n=1,args.n do
      if not args[n] then res=false break end
      local f=args[n]
      res=f(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
      if chk==0 or chkc then
        if not res then break end
      end
    end
    return res and true
  end
end

function Curry(f,...)
  local args=table.pack(...)
  return function(...)
    local a=table.pack(...)
    local t={}
    local c=0
    for n=1,args.n do
      t[n]=args[n]
      c=c+1
    end
    local l=c
    for n=1,a.n do
      t[n+c]=a[n]
      l=l+1
    end
    t.n=l
    if CheckDebugFlag() then return f(table.unpack(t)) end
    local r=table.pack(pcall(f,table.unpack(t)))
    local chk=r[1]
    table.remove(r,1)
    if not chk then error(r[1],2) return end
    return table.unpack(r)
  end
end

function CurryS(f,x)
  return function (...)
    return f(x,...)
  end
end

function CurryR(f,...)
  local args=table.pack(...)
  for n=1,args.n do
    f=CurryS(f,args[n])
  end
  return f
end

function CardCurry(f,...)
  local args={...}
  return function(c,...)
    local f=Curry(f,c,table.unpack(args))
    if CheckDebugFlag() then return f(...) end
    local r=table.pack(pcall(f,...))
    local chk=r[1]
    table.remove(r,1)
    if not chk then error(r[1],2) return end
    return table.unpack(r)
  end
end

function CardCurryS(f,x)
  return function (c,...)
    return f(c,x,...)
  end
end

function CardCurryR(f,...)
  local args=table.pack(...)
  for n=1,args.n do
    f=CardCurryS(f,args[n])
  end
  return f
end

--Do not use for now
function ChainInfoCurry(f,...)
  local args={...}
  local ci=C(Duel.GetChainInfo,0)
  local a=table.map(args,ci)
  return C(f,table.unpack(a))
end

function Flip(f)
  return function (arg1,arg2,...)
    return f(arg2,arg1,...)
  end
end

function Compose(...)
  local args=table.pack(...)
  return function(...)
    res=nil
    local first=true
    --for _,v in pairs(args) do
    for n=1,args.n do
      if first then
        res=args[n](...)
        first=false
      else
        res=args[n](res)
      end
    end
    return res
  end
end

-- DEPRECATED
function ValueOnlyFunction(f)
  return function(n,...)
    if n~=nil then
      Debug.Message(type(n))
      return f(n,...)
    end
  end
end

-- DEPRECATED
function OrderArguments(f,...)
  local args=table.pack(...)
  return function(...)
    local params=table.pack(...)
    for index=1,args.n do
      local swap=args[index]
      params[index],params[swap]=params[swap],params[index]
    end
    return f(table.unpack(params))
  end
end

function Reverse(f)
  return function(...)
    local args=table.pack(...)
    local t={}
    local n=args.n
    while n>0 do
      table.insert(t,args[n])
      n=n-1
    end
    f(table.unpack(t))
  end
end

function Negate(f)
  return function(...)
    return not f(...)
  end
end

function MaxBy(f,...)
  local args=table.pack(...)
  if args.n==0 then return nil end
  local r=args[1]
  for n=2,args.n do
    local c=args[n]
    if f(c) then
      if f(c)>f(r) or f(r)==nil then r=c end
    end
  end
  return r
end

function MinBy(f,...)
  local args=table.pack(...)
  if args.n==0 then return nil end
  local r=args[1]
  for n=2,args.n do
    local c=args[n]
    if f(c) then
      if f(r)==nil or f(c)<f(r) then r=c end
    end
  end
  return r
end

function WithHandler(f,...)
  local args={...}
  return function (e)
    return f(e:GetHandler(),table.unpack(args))
  end
end

function WithConfirm(tp,id,n,f,...)
  local hasConfirm=Duel.SelectYesNo(tp,aux.Stringid(id,n))
  if hasConfirm then return f(...) end
end

function Case(f,y,n,...)
  n=n or NoOperation
  local f1=CC(f,...)
  return function (x,...)
    if f1(x,...) then return y else return n end
  end
end

function NoOperation() end

function ShiftFunction(func,shift)
  shift=shift and shift-1 or 0
  if shift==-1 then return func end
  if shift<-1 then error(table.concat({"Shift Function : Second argument must be greater than 0. \"",shift+1,"\" was provided."})) end
  local f=function(_,...)
    return func(...)
  end
  return ShiftFunction(f,shift)
end

function ShiftArguments(func,shift)
  shift=shift and shift-1 or 0
  if shift==-1 then return func end
  if shift<-1 then error(table.concat({"Shift Arguments : Second argument must be greater than 0. \"",shift+1,"\" was provided."})) end
  local f=function(c,_,...)
    return func(c,...)
  end
  return ShiftArguments(f,shift)
end

function SingleShiftArgument(func,shift)
  shift=shift+1
  return function(c,...)
    local args=table.pack(...)
    return func(c,args[shift])
  end
end

-- I kind of forgot what this set of functions does.
-- I know it's some combination of ShiftFunctions and the DoFunctions commands
-- But I forget how the shifting works
local function ShiftArgumentDoFunction(func)
  return function (...)
    local f={}
    local args=table.pack(...)
    for n=1,args.n do
      table.insert(f,SingleShiftArgument(args[n],n-1))
    end
    return func(table.unpack(f))
  end
end
AndShiftFunctions=ShiftArgumentDoFunction(AndFunctions)
OrShiftFunctions=ShiftArgumentDoFunction(OrFunctions)
DoShiftFunctions=ShiftArgumentDoFunction(DoFunctions)

function PrepareFunction(f,...)
  local args=table.pack(...)
  return function()
    return f(table.unpack(args))
  end
end

function PrepareCardFunction(f,...)
  local args=table.pack(...)
  return function (c)
    return f(c,table.unpack(args))
  end
end

function Echo(...)
  return ...
end

function RecurseFilter(f,...)
  f=PrepareCardFunction(f,...)
  return function(c,...)
    if type(c)=="function" then
      return RecurseFilter(AndFunctions(f,PrepareCardFunction(c,...)))
    end
    return f(c,...)
  end
end

function AndNot(...)
  for _,v in pairs({...}) do
    if v then return false end
  end
  return true
end

function OrNot(...)
  for _,v in pairs({...}) do
    if not v then return true end
  end
  return false
end
