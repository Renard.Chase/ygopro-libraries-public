-- Vertex's Scripting Library
local io=require "io"
local src="duel-mode"
Extensions.Register(src,1,0,"Duel Mode Extension")
Extensions.Require(src,"config",1,0)

local mode=Config.GetValueOrDefault("duel_mode",nil)
if type(mode)~="string" then return end
mode="mode/"..mode

local function file_exists(file)
  local f=io.open(file)
  if f then f:close() end
  return f~=nil
end

if not file_exists(mode) then return end

local ce=Effect.CreateEffect
function Effect.CreateEffect(c,...)
  if mode then
    local f=dofile(mode)
    if f.load then f.load() end
    if f.ready then
      local e1=Effect.GlobalEffect()
      e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
      e1:SetCode(EVENT_PREDRAW)
      e1:SetProperty(NO_COPY_NO_DISABLE+EFFECT_FLAG_IMMEDIATELY_APPLY+EFFECT_FLAG_IGNORE_IMMUNE)
      e1:SetCountLimit(1)
      e1:SetOperation(function (e,tp,eg,ep,ev,re,r,rp)
        f.ready()
        e:Reset()
      end)
      Duel.RegisterEffect(e1,0)
    end
    mode=nil
  end
  return ce(c,...)
end
