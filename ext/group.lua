local src="group"
Extensions.Register(src,1,3,"Group")
Extensions.Require(src,"type",1,0)
Extensions.Require(src,"function",1,0)
Extensions.Require(src,"math",1,4)
Extensions.Require(src,"maybe",1,0)

local function getExcept(x) local t=type(x) if t=="nothing" then return nil elseif t:sub(1,4)=="just" then return x:GetValue() else return x or nil end
end

function Group.Fold(g,f,base,...)
  if g:IsEmpty() then return base end
  local result=base
  local tc=g:GetFirst()
  while tc do
    result=f(result,tc,...)
    tc=g:GetNext()
  end
  --[[g:ForEach(function (tc)
    result=f(result,tc,args)
  end)]]
  return result
end

function Group.Map(g,f,...)
  local result={}
  local args=table.pack(...)
  g:ForEach(function (tc)
    table.insert(result,f(tc,table.unpack(args)))
  end)
return result
end

function Group.MapGroup(g,f,...)
  local args=table.pack(...)
  local result=Group.CreateGroup()
  g:ForEach(function (tc)
    result:AddCards(f(tc,args))
  end)
  return result
end
Group.MapG=Group.MapGroup

function Group.All(g,f,...)
  return g:IsExists(f,g:GetCount(),nil,...)
end

function Group.AllOf(g,filter,check,...)
  local g2=nil
  if type(filter)=="table" then
    local f=function(c,fl,...)
      return fl(c,...)
    end
    g2=g:Filter(f,nil,table.unpack(filter))
  else
    g2=g:Filter(filter,nil)
  end
  return g2:All(check,...)
end

function Group.AllWith(g,f,c,...)
  local t=g:Map(f)
  for _,v in pairs(t) do
    if type(c)=="function" then
      if not c(v,...) then return false end
    else
      if c~=v then return false end
    end
  end
  return true
end

function Group.AllMatch(g,c,f,...)
  local v=f(c,...)
  return g:All(Compose(f,math.equal(v)),...)
end

function Group.Any(g,f,...)
  return g:IsExists(f,1,nil,...)
end

function Group.AnyOf(g,filter,check,...)
  local g2=nil
  if type(filter)=="table" then
    local f=function(c,fl,...)
      return fl(c,...)
    end
    g2=g:Filter(f,nil,table.unpack(filter))
  else
    g2=g:Filter(filter,nil)
  end
  return g2:Any(check,...)
end

function Group.AnyWith(g,f,c,...)
  local t=g:Map(f)
  for _,v in pairs(t) do
    if type(c)=="function" then
      if c(v,...) then return true end
    else
      if c==v then return true end
    end
  end
  return false
end

function Group.AnyMatch(g,c,f,...)
  local v=f(c,...)
  return g:Any(Compose(f,math.cequal(v)))
end

Group.None=Negate(Group.Any)
Group.NoneOf=Negate(Group.AnyOf)
Group.NoneWith=Negate(Group.AnyWith)
Group.NoMatch=Negate(Group.AnyMatch)

function Group.Partition(g,f,...)
  local include=Group.CreateGroup()
  local exclude=Group.CreateGroup()
  local args=table.pack(...)

  g:ForEach(function (tc)
    if f(tc,table.unpack(args)) then
      include:AddCard(tc)
    else
      exclude:AddCard(tc)
    end
  end)
  return include,exclude
end

function Group.FoldPartition(g,pf,ff,b,...)
  local i,x=g:Partition(pf,...)
  
  local f=CC(Group.Fold,ff,b)
  local ir,xr=f(i),f(x)

  return ir,xr
end

function Group.WithParitions(...)
  Debug.Message("WARNING : Groupt.WithParitions called")
  return Group.WithPartitions(...)
end

function Group.WithPartitions(g,pf,wf,...)
  local i,x=g:Partition(pf,...)
  local ir,xr=wf(i),wf(x)
  return ir,xr
end

local function ceq(c1,c2)
  return c1==c2
end

function Group.IntersectBy(g1,g2,f)
  return g1:DropUnless(function (tc)
    return g2:Any(function (cc)
      return f(tc,cc)
    end)
  end)
end

local fe=Group.ForEach
function Group.ForEach(g,f,...)
  local args=table.pack(...)
  fe(g,function (tc)
    f(tc,table.unpack(args))
  end)
end
Group.Sequence=Group.ForEach

function Group.LoopFor(g,f,...)
  local args=table.pack(...)
  local tc=g:GetFirst()
  while tc do
    f(tc,table.unpack(args))
    tc=g:GetNext()
  end
end

function Group.ForEachWith(g,t,f,...)
  local args=table.pack(...)
  g:ForEach(function (tc)
    f(t(tc),table.unpack(args))
  end)
end

function Group.Intersect(g1,g2)
  return g1:IntersectBy(g2,ceq)
end

local function diff_check(f,g1,g2)
  return g1:DropUnless(function (c)
    return g2:None(function (d)
      return f(c,d)
    end)
  end)
end


function Group.DifferenceByM(g1,g2,f)
  local func=function (g01,g02) return diff_check(f,g01,g02) end
  local r1=func(g1,g2)
  local r2=func(g2,g1)
  return r1,r2
end

function Group.DifferenceBy(g1,g2,f)
  local r1,r2=g1:DifferenceByM(g2,f)
  return r1:Join(r2)
end

function Group.DifferenceThisBy(g1,g2,f)
  local func=function (g01,g02) return diff_check(f,g01,g02) end
  return func(g1,g2)
end

function Group.Difference(g1,g2)
  return g1:DifferenceBy(g2,ceq)
end

function Group.DifferenceM(g1,g2)
  return g1:DifferenceByM(g2,ceq)
end

function Group.DifferenceThis(g1,g2)
  return g1:DifferenceThisBy(g2,ceq)
end

local ac=Group.AddCard
function Group.AddCard(g,...)
  local args=table.pack(...)
  for n=1,args.n do
    if args[n] then ac(g,args[n]) end
    n=n+1
  end
end
Group.AddCards=Group.AddCard

function Group.InsertCard(g,...)
  local ng=g:Clone()
  ng:AddCard(...)
  return ng
end

function Group.IncludeCard(g,...)
  g:AddCard(...)
  return g
end

local rc=Group.RemoveCard
function Group.RemoveCard(g,...)
  local args=table.pack(...)
  for n=1,args.n do
    local x=args[n]
    if type(x)=="card" then
      rc(g,x)
    elseif type(x)=="group" then
      x:ForEach(Curry(Group.RemoveCard,g))
    elseif type(x)=="number" then
      g:RemoveIf(CardCurry(Card.IsCode,x))
    elseif type(x)=="function" then
      g:RemoveIf(x)
    elseif type(x)=="table" then
      for _,v in ipairs(x) do
        g:RemoveCard(v)
      end
    end
  end
end

function Group.DropCard(g,...)
  local ng=g:Clone()
  ng:RemoveCard(...)
  return ng
end

function Group.ExcludeCard(g,...)
  g:RemoveCard(...)
  return g
end

function Group.RemoveIf(g,f,...)
  local r=g:Filter(f,nil,...)
  g:RemoveCard(r)
end

function Group.RemoveUnless(g,f,...)
  g:RemoveIf(Negate(f),...)
end

function Group.DropIf(g,...)
  local ng=g:Clone()
  ng:RemoveIf(...)
  return ng
end

function Group.DropUnless(g,...)
  local ng=g:Clone()
  ng:RemoveUnless(...)
  return ng
end

function Group.ExcludeIf(g,...)
  g:RemoveIf(...)
  return g
end

function Group.ExcludeUnless(g,...)
  g:RemoveUnless(...)
  return g
end

function Group.AsTable(g)
  return g:Map(function (c) return c end)
end

function Group.CreateFromTable(t)
  local g=Group.CreateGroup()
  for _,v in pairs(t) do
    g:AddCard(v)
  end
  return g
end
Group.FromTable=Group.CreateFromTable

function Group.ConfirmCards(tg,tp)
  if type(tg)=="number" then return Curry(Duel.ConfirmCards,tg) end
  Duel.ConfirmCards(tp,tg)
end

function Group.ConfirmtoHand(target,player,reason)
  if type(target)=="number" then return CC(Card.ConfirmtoHand,target,player) end
  local n=target:SendtoHand(player,reason)
  target=Duel.GetOperatedGroup()
  if player~=nil then
    target:ConfirmCards(1-player)
  else
    local p1,p2=target:Partition(Card.IsControler,1)
    p1:ConfirmCards(0)
    p2:ConfirmCards(1)
  end
  return n
end

function Group.Sort(g,f)
  local t=g:AsTable()
  local n=Group.CreateGroup()
  table.sort(t,f)
  for _,v in ipairs(t) do
    n:AddCard(v)
  end
  return n
end

function Group.Take(g,n)
  local c=g:GetFirst()
  local r=Group.CreateGroup()
  local a=0
  while a<n do
    r:AddCard(c)
    c=g:GetNext()
    a=a+1
  end
  return r
end

function Group.GetFirstMatch(g,f,...)
  local tc=g:GetFirst()
  local r=nil
  while((not r) and (tc)) do
    r=f(tc,...) and tc
    tc=g:GetNext()
  end
  return r
end

function Group.MaybeGetFirstMatch(g,f,...)
  local r=g:GetFirstMatch(f,...)
  return r and Maybe.Just(r) or Maybe.Nothing()
end

function Group.MaybeGetFirst(g)
  local tc=g:GetFirst()
  return tc and Maybe.Just(tc) or Maybe.Nothing()
end

function Group.MaybeGetNext(g)
  local tc=g:GetNext()
  return tc and Maybe.Just(tc) or Maybe.Nothing()
end

function Group.Destroy(g,r,d)
  r=r or REASON_EFFECT
  return Duel.Destroy(g,r,d)
end

function Group.Banish(g,p,r)
  r=r or REASON_EFFECT
  p=p or POS_FACEUP
  return Duel.Remove(g,p,r)
end

function Group.BanishAsCost(g,p,r)
  r=(r or 0)+REASON_COST
  return g:Banish(p,r)
end

function Group.SendtoGrave(target,reason)
  reason=reason or REASON_EFFECT
  return Duel.SendtoGrave(target,reason)
end

function Group.SendtoGraveAsCost(target,reason)
  reason=(reason or 0)+REASON_COST
  target:SendtoGrave(reason)
end

function Group.Discard(target,reason)
  reason=reason or REASON_EFFECT
  return target:SendtoGrave(reason+REASON_DISCARD)
end

function Group.DiscardAsCost(target,reason)
  reason=(reason or 0)+REASON_DISCARD
  return target:SendtoGraveAsCost(reason)
end

function Group.SendtoHand(target,player,reason)
  reason=reason or REASON_EFFECT
  return Duel.SendtoHand(target,player,reason)
end

function Group.SendtoHandAsCost(target,player,reason)
  reason=(reason or 0)+REASON_COST
  target:SendtoHand(player,reason)
end

function Group.SendtoDeck(target,player,seq,reason)
  seq=seq or 0
  reason=reason or REASON_EFFECT
  return Duel.SendtoDeck(target,player,seq,reason)
end

function Group.SendtoDeckAsCost(target,player,seq,reason)
  reason=(reason or 0)+REASON_COST
  return target:SendtoDeck(player,seq,reason)
end

function Group.ShuffletoDeck(target,player,reason)
  reason=reason or REASON_EFFECT
  return target:SendtoDeck(player,2,reason)
end

function Group.ShuffletoDeckAsCost(target,player,reason)
  reason=(reason or 0)+REASON_COST
  return target:ShuffletoDeck(player,reason)
end

function Group.Release(g,r)
  r=r or REASON_COST
  return Duel.Release(g,r)
end

function Group.SpecialSummon(target,sumtype,sumplayer,target_player,nocheck,nolimit,pos)
  sumtype=sumtype or 0
  target_player=target_player or sumplayer
  nocheck=nocheck or false
  nolimit=nolimit or false
  pos=pos or POS_FACEUP
  --[[local f = function (tc)
    local p=sumplayer or tc:GetControler()
    local tp=target_player or p
    return Duel.SpecialSummonStep(tc,sumtype,p,tp,nocheck,nolimit,pos)
  end
  local tc=target:GetFirst()
  local n=0
  while (tc) do
    n=n+(f(tc) and 1 or 0)
    tc=target:GetNext()
  end
  Duel.SpecialSummonComplete()
  return n]]
  return Duel.SpecialSummon(target,sumtype,sumplayer,target_player,nocheck,nolimit,pos)
end

function Group.SpecialSummonStep(target,sumtype,sumplayer,target_player,nocheck,nolimit,pos)
  sumtype=sumtype or 0
  nocheck=nocheck or false
  nolimit=nolimit or false
  pos=pos or POS_FACEUP
  return Duel.SpecialSummonStep(target,sumtype,sumplayer,target_player,nocheck,nolimit,pos)
end

function Group.IsEmpty(g)
  return g:GetCount()==0
end

function Group.HasCard(g)
  return not g:IsEmpty()
end

function Group.HasCount(g,count)
  return g:GetCount()>=count
end

function Group.IsUnderCount(g,count)
  return not g:HasCount(count)
end

local m=Group.Merge
function Group.Merge(g,...)
  local r=nil
  local args=table.pack(...)
  for n=1,args.n do
    local d=args[n]
    if type(d)=="card" then
      r=m(g,Group.FromCards(d))
    elseif type(d)=="effect" then
      r=m(g,Group.FromCards(d:GetHandler()))
    elseif type(d)=="function" then
      local x=Duel.GetMatchingGroup(d,tp,0xFF,0xFF,nil)
      r=m(g,x)
    elseif type(d)=="just card" then
      local x=d:WithValueOrDefault(Group.CreateGroup,Group.CreateGroup())
      r=m(g,x)
    else
      r=m(g,d)
    end
  end
  return r
end

function Group.MergeIf(g1,g2,f,...)
  local filter=CC(f,...)
  g2:ForFilter(f,Group.AddCard,g1)
end

function Group.ForFilter(g,f,a,...)
  g:ForEach(function (tc,...)
    if f(tc,...) then a(tc) end
  end,...)
end

function Group.MaxBy(g,f,...)
  return g:Fold(function (acc,tc,...) return f(tc,...)>f(acc,...) and tc or acc end,g:GetFirst(),...)
end

function Group.MinBy(g,f,...)
  return g:Fold(function (acc,tc,...) return f(tc,...)<f(acc,...) and tc or acc end,g:GetFirst(),...)
end

function Group.SumBy(g,f,b,...)
  b=b or 0
  return g:Fold(function (acc,tc,...) return acc+f(tc,...) end,b,...)
end

function Group.SubBy(g,f,b,...)
  b=b or 0
  return g:Fold(function (acc,tc,...) return acc-f(tc,...) end,b,...)
end

function Group.MaybeMaxBy(g,f,...)
  return g:Fold(function (acc,tc,...)
    if not acc:HasValue() then return Maybe.Just(tc) end
    return f(tc,...)>f(acc:GetValue(),...) and Maybe.Just(tc) or acc
  end,Maybe.Nothing(),...)
end

function Group.MaybeMinBy(g,f,...)
  return g:Fold(function (acc,tc,...)
    if not acc:HasValue() then return Maybe.Just(tc) end
    return f(tc,...)<f(acc:GetValue(),...) and Maybe.Just(tc) or acc
  end,Maybe.Nothing(),...)
end

function Group.FoldWith(g,af,vf,b)
  return g:Fold(function (acc,x) return af(acc,vf(x)) end,b)
end

local gc=Group.GetCount
function Group.GetCount(g,arg1,...)
  if type(arg1)=="number" then
    return gc(g)*arg1
  elseif type(arg1)=="function" then
    return arg1(gc(g),...)
  else
    return gc(g,arg1,...)
  end
end

function Group.NubWith(g,f,...)
  local r=Group.CreateGroup()
  local args={...}
  g:ForEach(function (tc)
    if not r:Any(function (c) return f(tc,c,table.unpack(args)) end) then
      r:AddCard(tc)
    end
  end)
  return r
end

function Group.NubBy(g,f,...)
  local chk=function (tc,ex,...) return f(tc,...)==f(ex,...) end
  return g:NubWith(chk,...)
end

function Group.Nub(g)
  return g:NubBy(Card.GetCode)
end

function Group.HasClassCount(g,f,n,...)
  return g:GetClassCount(f,...)>=n
end

function Group.IsUnderClassCount(g,f,n,...)
  return not g:HasClassCount(f,n,...)
end

function Group.Join(g,...)
  local r=Group.CreateGroup()
  r:Merge(g,...)
  return r
end

local fs=Group.FilterSelect
function Group.FilterSelect(g,p,f,min,max,x,...)
  min=min or 1
  max=max or min
  x=getExcept(x)
  return fs(g,p,f,min,max,x,...)
end

function Group.WithFilterSelection(g,f,p,fil,min,max,x,...)
  local tg=g:FilterSelect(p,fil,min,max,x,...)
  local bool,res=pcall(f,tg)
  if not bool then error(res,2) end
  --return f(tg)
  return res
end

local s=Group.Select
function Group.Select(g,p,min,max,ex)
  min=min or 1
  max=max or min
  ex=getExcept(ex)
  return s(g,p,min,max,ex)
end

function Group.SelectSingleCard(g,p,ex)
  ex=getExcept(ex)
  return g:Select(p,1,1,ex):GetFirst()
end

function Group.WithSelection(g,f,p,min,max,ex)
  local tg=g:Select(p,min,max,ex)
  return f(tg)
end

function Group.WithSingleCard(g,f,p,ex)
  return g:WithSelection(CMP(Group.GetFirst,f),p,1,1,ex)
end

function Group.FilterSetCode(g,sc)
  local fg=g:Filter(Card.IsSetCard,nil,sc)
  return fg
end

function Group.WithGroup(g,f,...)
  if g:HasCard() then
    return f(g,...)
  end
end

function Group.HintSelection(g)
  Duel.HintSelection(g)
end

Group.Head=Group.GetFirst
function Group.Tail(g)
  return g:DropCard(g:Head())
end

function Group.Last(g)
  local tc=g:GetFirst()
  local c=tc
  while tc do
    c=tc
    tc=g:GetNext()
  end
  return c
end

function Group.Init(g)
  return g:DropCard(g:Last())
end

function Group.SSet(g,tp,ttp)
  return Duel.SSet(tp,g,ttp)
end

function Group.ChangePosition(g,au,ad,du,dd,noflip,setavailable)
  return Duel.ChangePosition(g,au,ad,du,dd,noflip,setavailable)
end

local swse=Group.SelectWithSumEqual
function Group.SelectWithSumEqual(g,p,f,s,mn,mx,...)
  if mx>=128 then
    error("Group.SelectWithSumEqual : Maximum selection values of 128 or above will cause the game to crash.("..mx.." was provided)",2)
  end
  local b,m=pcall(swse,g,p,f,s,mn,mx,...)
  if not b then error(m,2) end
  return m
end

Group.ReleaseRitualMaterial=Duel.ReleaseRitualMaterial

function Group.RandomSelectSingle(g,tp)
  return g:RandomSelect(tp,1):GetFirst()
end

function Group.OperationInfo(g,c,ct,n)
  if type(g)~="Group" then return CC(Group.OperationInfo,g,n,c) end
  n=n or 0
  ct=ct or g:GetCount()
  Duel.SetOperationInfo(n,c,g,ct,0,0)
end

function Group.Selection(g,...)
  local sg=g:Select(...)
  g:Clear()
  g:Merge(sg)
  return g
end

function Group.ReverseInDeck(g)
  g:LoopFor(Card.ReverseInDeck)
  return g
end

function Group.Iterator(g)
  local isFirst=true
  return function ()
    if isFirst then
      isFirst=false
      return g:GetFirst()
    end
    return g:GetNext()
  end
end

function Group.RandomIterator(g,tp)
  return function ()
    local tc=g:RandomSelectSingle(tp)
    if tc then g:RemoveCard(tc) end
    return tc
  end
end

function Group.GetControl(c,p,r,t)
  return Duel.GetControl(c,p,r,t)
end

function Group.WithRandomSelect(g,f,tp,n,...)
  local tg=g:RandomSelect(tp,n)
  return f(tg,...)
end

function Group.WithRandomSingle(g,f,tp,...)
  local tc=g:RandomSelectSingle(tp)
  return f(tc,...)
end
